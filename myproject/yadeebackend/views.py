from django.contrib.auth.models import User
from rest_framework import viewsets
from .serializers import UserSerializer,CouponCodeSerializer,AnonymousShopperSerializer, RegisterEventSerializer, AvatarImageSerializer,LineItemSerializer, ResetCodePackSerializer, ShippingModeSerializer, UserProfileSerializer, LoginUserSerializer, CreateUserSerializer, ProductSerializer, AttributeTypeSerializer, AttributeValueSerializer, TransactionSerializer, CategorySerializer, AttributeSerializer,  ImageSerializer, AddressSerializer, SellerSerializer, YediShopperSerializer, YediShopSerializer, OrderSerializer
from .models import Product, Category,PaymentMethod,CouponCode,AnonymousShopper, RegisterEvent, ResetCodePack, ShippingMode,LineItem, AvatarImage, LogoImage, Address, UserProfile, AttributeValue,AttributeType, Seller, User,Image,Attribute, Shopper, YediShop, Order, BankDetails, Bank, Transaction
from rest_framework.response import Response
from rest_framework import status
from datetime import datetime, date
from dateutil.parser import parse

from django.http import HttpResponse
from cloudant.client import Cloudant
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from django.db import IntegrityError

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view, authentication_classes, permission_classes

from rest_framework.authtoken.models import Token
from rest_framework import generics
from rest_framework import viewsets,views
from django.views.decorators.csrf import csrf_exempt
from .pushnotifications import send_push_message
import random
import string
import rollbar
from dateutil.parser import parse
from . import emails
from django.core.exceptions import ObjectDoesNotExist
from django.utils.crypto import get_random_string
import emailer.SMS as smsbot
from .permissions import IsCustomer, IsStaff

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
   return ''.join(random.choice(chars) for _ in range(size))

try:
    for user in User.objects.all():
        Token.objects.get_or_create(user=user)

except Exception as e:
     print( '%s (%s)' % (str(e), type(e)))





cloudantDB = 'yadee_prod'
cloudantUsersDB = '_users'


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

class AttributeTypeViewSet(viewsets.ModelViewSet):
    queryset = AttributeType.objects.all()
    serializer_class = AttributeTypeSerializer

class AttributeValueViewSet(viewsets.ModelViewSet):
    queryset = AttributeValue.objects.all()
    serializer_class = AttributeValueSerializer

class AttributeViewSet(viewsets.ModelViewSet):
    queryset = Attribute.objects.all()
    serializer_class = AttributeSerializer

class LineItemViewSet(viewsets.ModelViewSet):
    queryset = LineItem.objects.all()
    serializer_class = LineItemSerializer

class ResetCodePackViewSet(viewsets.ModelViewSet):
    queryset = ResetCodePack.objects.all()
    serializer_class = ResetCodePackSerializer

    def create(self, request, *args, **kwargs):
        try:
            if len(list(request.data)) > 1:
                email = request.data['email'].lower()
                rcp = ResetCodePack.objects.get(resetcode = request.data['resetcode'])
                if rcp.email.lower() == request.data['email']:
                    user = User.objects.get(email__iexact = email)
                    user.set_password(request.data['password'])
                    user.save()
                    emails.send_passwordchangedone_email(email, user.username)
                    rcp.delete()
                    return HttpResponse('Password changed successfully')
                return HttpResponse('Reset Code is case sensitive')
            else:
                users = User.objects.filter(email__iexact = request.data['email'])
                unique_id = get_random_string(length=5)
                if len(users) > 0:
                    rc = ResetCodePack(email=request.data['email'], resetcode=unique_id)
                    rc.save()
                    emails.send_resetcode_email(rc.resetcode, rc.email)
                    profiles = UserProfile.objects.filter(user__email=rc.email.lower())
                    smsbot.send_resetcodepack_sms(rc.resetcode, profiles[0].phone)
                    return HttpResponse(unique_id)
                return HttpResponse('User with email not found')
        except ObjectDoesNotExist as e:
            rollbar.report_exc_info(extra_data={'error': e, 'message': e, 'user': request.data })
            return Response({'error': 'Error changing password: ' + str(e)}, status=409)

class ProductViewSet(viewsets.ModelViewSet):
    # queryset = Product.objects.all().prefetch_related('categories').order_by('-price')
    serializer_class = ProductSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter)
    filter_fields = ('name', 'seller__user__username' )#'price', 'isfeatured', 'seller', 'categories', 'categories__parent')
    search_fields = ('seller__user__email', 'seller__user__username', 'seller__shop__name','name', 'description')
    #permission_classes = (IsCustomer,)

    def get_queryset(self):

        cats = self.request.query_params.get('categories', None)
        searchterm = self.request.query_params.get('search', None)
        seller = self.request.query_params.get('seller', None)
        if cats:
            lowerprods =   Product.objects.filter(categories__in=[cats])
            midprods =  Product.objects.filter(categories__parent=cats)
            return  midprods | lowerprods
        elif searchterm:
            print('searched: '+searchterm)
            byname = Product.objects.filter(name__icontains=searchterm)
            byseller = Product.objects.filter(seller__user__username__icontains=searchterm.lower())
            byshop = Product.objects.filter(seller__shop__name__icontains=searchterm)
            return  byname|byseller|byshop
        elif seller:
            byseller = Product.objects.filter(seller=seller)
            return byseller
        else:
            # return Product.objects.filter(isdeleted=False).order_by('seller')
            # return Product.objects.filter(name_icontains=241).order_by('created')
            return Product.objects.filter(isdeleted=False)

    # def paginate_queryset(self, queryset):
    #     page = super(ProductViewSet, self).paginate_queryset(queryset)
    #     random.shuffle(page)
    #     return page

    def create(self, request, *args, **kwargs):
        try:
            if 'id' in request.data.keys():
                prod = Product.objects.get(id=request.data['id'])
                prod.price = request.data['price']
                prod.description = request.data['description']
                prod.name = request.data['name']
                prod.on_sale = request.data['on_sale']
                prod.in_stock = request.data['in_stock']
                prod.save()
                return Response({'success': True, 'ok' : True, })
            prod = request.data
            seller = UserProfile.objects.get(id=prod['seller'])
            try:
                if 'pushtoken' in request.data.keys():
                    seller.pushtoken = request.data['pushtoken']
                    seller.save()
            except Exception as e:
                rollbar.report_exc_info(extra_data={'error': e, 'message': e, 'user': UserProfile.objects.get(id = request.data['seller']).user.username })

            cat = Category.objects.get(id=prod['categories'])
            newp = Product(name=prod['name'], description=prod['description'], price=prod['price'], weightstr=prod['weight'], on_sale=prod['on_sale'], in_stock=prod['in_stock'], seller=seller )
            newp.save()
            newp.categories.add(cat)
            newp.save()



            # send_mail('New Product', ('You have Added a new product'+ newp.name ), 'postmaster@yedi.com.ng', ['f.galaudu@gmail.com'])

            return Response({'success': newp.id, 'ok' : True, })
        except Exception as e:
            rollbar.report_exc_info(extra_data={'error': e, 'message': e, 'user': UserProfile.objects.get(id = request.data['seller']).user.username })
            return Response({'error': 'Error Creating/Editing Product' + str(e)}, status=409)



    #
    # def create(self, request, *args, **kwargs):
    #     serializer = self.get_serializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     self.perform_create(serializer)
    #     headers = self.get_success_headers(serializer.data)
    #     return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.exclude(parent__name='FrontPage').exclude(name="FrontPage").exclude(name="Sticky").order_by('name')
    serializer_class = CategorySerializer

class AddressViewSet(viewsets.ModelViewSet):
    queryset = Address.objects.all()
    serializer_class = AddressSerializer

class AvatarImageViewSet(viewsets.ModelViewSet):
    queryset = AvatarImage.objects.all()
    serializer_class = AvatarImageSerializer

class UserProfileViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

    def create(self, request, *args, **kwargs):
        try:
            if 'pushtoken' in request.data.keys():
                #if 'profile' in request.data.keys():
                profile = UserProfile.objects.get(id = request.data['profile'])
                profile.pushtoken = request.data['pushtoken']
                profile.save()

                return Response({'success': True, 'ok' : True, })
                #return Response( 'no user signed up')
                #rollbar.report_exc_info(extra_data={'error': e })
            return Response('no push token attaached')
        except Exception as e:
            return Response('no push token attaached', status=409)
            rollbar.report_exc_info(extra_data={'error': e, 'user': request.data['profile']})


class SellerViewSet(viewsets.ModelViewSet):
    queryset = Seller.objects.all()
    serializer_class = SellerSerializer

class YediShopperViewSet(viewsets.ModelViewSet):
    queryset = Shopper.objects.all()
    serializer_class = YediShopperSerializer

class ShippingModeViewSet(viewsets.ModelViewSet):
    queryset = ShippingMode.objects.all()
    serializer_class = ShippingModeSerializer



class YediShopViewSet(viewsets.ModelViewSet):
    queryset = YediShop.objects.all()
    serializer_class = YediShopSerializer


class CouponCodeViewSet(viewsets.ModelViewSet):
    queryset = CouponCode.objects.all()
    serializer_class = CouponCodeSerializer


class RegisterEventViewSet(viewsets.ModelViewSet):
    queryset = RegisterEvent.objects.all()
    serializer_class = RegisterEventSerializer


class AnonymousShopperViewSet(viewsets.ModelViewSet):
    queryset = AnonymousShopper.objects.all()
    serializer_class = AnonymousShopperSerializer


class OrderViewSet(viewsets.ModelViewSet):
    # queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def get_queryset(self):
        shopper = self.request.query_params.get('customer', None)
        if shopper:
            user = UserProfile.objects.get(id=shopper)
            userorders =   Order.objects.filter(customer=user)
            return  userorders
        return Order.objects.all()

    def create(self, request, *args, **kwargs):
        try:
            user = UserProfile.objects.get(id=request.data['customer_id'])
            #if not user.phone:
            if 'phone' in request.data['shipping'].keys():
                user.phone = request.data['shipping']['phone']
                user.save()

            comment = request.data['customer_note']

            paymentmethod, created1 = PaymentMethod.objects.get_or_create(name=request.data['payment_method'])
            shippingmethod, created2 = ShippingMode.objects.get_or_create(mode='delivery')
            name  = request.data['shipping']['last_name'] + request.data['shipping']['first_name']
            address  = request.data['shipping']['address_1']
            state  = request.data['shipping']['state']
            totalprice  = request.data['totalPrice']
            if 'shippingcost' in request.data.keys():
                totalprice  = request.data['shippingcost']
            if 'shippingcost' in request.data.keys():
                totalprice  = request.data['shippingcost']
            addy = Address(name=name, line1=address, state=state)
            addy.save()
            oid = get_random_string(length=3)
            order = Order(customer=user,
                          comments=comment,
                          shippingmode=shippingmethod,
                          address=addy,
                          totalprice=totalprice,
                          status='NEW')
            order.save()
            order._id = str("YC"+oid.upper()+str(order.id))
            order.save()

            for lineitem in request.data['line_items']:
                pid = lineitem['product_id']
                quantity = lineitem['quantity']
                pp = Product.objects.prefetch_related('images').get(id=pid)
                li = LineItem(product=pp,
                         quantity=quantity,
                         order=order)
                li.save()

                t = Transaction(line_item=li, status='NEW')
                try:
                    if li.product.seller.phone:
                        smsbot.send_transaction_sms(number=li.product.seller.phone, product=li.product.name, quantity=li.quantity)
                    send_push_message(li.product.seller.pushtoken,'Congrats! You have a new transaction in yedi for Product: ' + li.product.name + ". Go into Your transactions to approve or decline the order!")
                except Exception as e :
                    rollbar.report_exc_info(extra_data={'error': e, 'user': user.user.username })
                emails.send_tranaction_email(order,li,li.product.seller.user)

                t.save()
                p = t.line_item.product.images

            emails.send_new_order_email(neworder=order)
            smsbot.send_neworder_sms(number=user.phone, orderid=order._id)
            paymentmethod.order = order
            paymentmethod.save()
            return Response({'success': order.id, 'ok' : True})
        except Exception as e:
            rollbar.report_exc_info(extra_data={'error': e, 'user': request.data['customer_id'] })
            return Response({'error': 'Error Creating Order' + str(e)}, status=409)

class ImageViewSet(viewsets.ModelViewSet):
    # queryset = Image.objects.all()
    serializer_class = ImageSerializer

    def get_queryset(self):
        prodid = self.request.query_params.get('prodid', None)
        if prodid:
            prodimages = Image.objects.filter(product_id=prodid)
            return prodimages
        return Image.objects.all()


class TransactionViewSet(viewsets.ModelViewSet):
    serializer_class = TransactionSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter)
    filter_fields = ('line_item', 'line_item__product', 'line_item__order' )#'price', 'isfeatured', 'seller', 'categories', 'categories__parent')
    search_fields = ('line_item', 'line_item__product', 'line_item__order')

    def get_queryset(self):
        seller = self.request.query_params.get('seller', None)
        if seller:
            sellertrans = Transaction.objects.filter(line_item__product__seller__id=seller)
            return sellertrans
        return Transaction.objects.all()

    def create(self, request, *args, **kwargs):
        try:
            if 'id' in request.data.keys():
                trans = Transaction.objects.get(id=request.data['id'])
                trans.status = request.data['status']
                trans.save()
                try:
                    send_push_message(trans.line_item.order.customer.pushtoken,'Product :' + trans.line_item.product.name + " from your Order: " + str(trans.line_item.order.id) +" has been updated to: " + trans.status)
                    send_push_message("ExponentPushToken[VTfFuVMCKMZBpTJwJ6BWFa]",'INFO: Product :' + trans.line_item.product.name + " from Order: " + str(trans.line_item.order.id) +" has been updated to: " + trans.status)
                    if trans.status == 'Accepted':
                        trans.line_item.order.status = trans.status
                        trans.order.save()
                except Exception as e:
                    rollbar.report_exc_info(extra_data={'error': e, 'token': trans.line_item.order.customer.pushtoken })

                return Response({'success': True, 'ok' : True, })
            return Response({'success': True, 'ok' : True, })
        except Exception as e:
            rollbar.report_exc_info(extra_data={'error': e})
            return Response({'error': 'Error Editing Transaction' + str(e)}, status=409)

@csrf_exempt
def post_uploadimg(request):
    try:
        if request.method == 'GET':
            return HttpResponse("Uploader")
        elif request.method == 'POST':
            print(request.POST['profile'])
            profile = UserProfile.objects.get(id=(request.POST['profile']))
            if request.POST['type'] == 'avatar':
                ava = AvatarImage(image = request.FILES['photo'] )
                ava.save()
                profile.avatar = ava
                profile.save()
            elif request.POST['type'] == 'productimage' :
                prd = Product.objects.get(id=request.POST['product'])
                img = Image(image = request.FILES['photo'], product=prd)
                img.save()
            #storedimg = settings.STATIC_URL+ "media/Images/AvatarImages/"+(request.FILES['photo']+id_generator()).replace('(','').replace(')','').replace('&','').replace(' ','_')
            return HttpResponse('success')
    except Exception as e:
        return HttpResponse('fail')



class LoginUserView(generics.CreateAPIView):
    serializer_class=LoginUserSerializer
    def post(self, request, format=None):
        if "@" in request.data['username']:
            users=User.objects.filter(email__iexact=request.data['username'])
        else:
            users=User.objects.filter(username__iexact=request.data['username'])

        if users:
            if users[0].check_password(request.data['password']):
                token,created=Token.objects.get_or_create(user=users[0])
                profile = UserProfile.objects.get(user=users[0])
                response={'token': token.key,'user': users[0].id, 'profile': profile.id}

                user=users[0]
                try:
                    if (request.data['pushtoken']):
                        profile.pushtoken = request.data['pushtoken']
                        profile.save()
                    print(str(profile))
                    #response.update({ 'profile' : user.profile.id})
                except:
                    pass

                # try:
                #     response.update({ 'seller' : user.seller.id})
                # except Exception as e:
                #     print(e)

                return Response(response)
            rollbar.report_exc_info(extra_data={'error': users[0], 'status': '401', 'message': 'incorrect password', 'username': request.data['username']})
            return Response({'error' : 'Incorrect password'},status=401)
        rollbar.report_exc_info(extra_data={' ': 'user not found', 'username': request.data['username']})
        return Response({'error' : 'User not found'},status=404)

class CreateUserView(generics.CreateAPIView):
    serializer_class=CreateUserSerializer
    def post(self, request, format=None):
        try:

            users=User.objects.filter(email=request.data['email'])
            usernameexitst=User.objects.filter(username=request.data['username'])
            print(users)
            if not users and  not usernameexitst:
                user=User(username=request.data['username'],
                          first_name=request.data['first_name'],
                          last_name=request.data['last_name'],
                          email=request.data['email'],
                )
                user.set_password(request.data['password'])
                user.is_staff=False
                user.save()
                profile = UserProfile(user = user)
                profile.save()
                profile.pushtoken = request.data['pushtoken']
                profile.save()
                if 'phone' in request.data.keys():
                    profile.phone = request.data['phone']
                    profile.save()
                token,created=Token.objects.get_or_create(user=user)
                if 'isSeller' in request.data.keys():
                    if request.data['isSeller'] == True:
                        shop =YediShop(name = request.data["shopname"], isactive=True, seller=profile)
                        shop.save()
                        profile.isSeller = True
                    else:
                        profile.isSeller = False
                profile.save()
                emails.send_signup_email(user, profile)
                return Response({'token': token.key, 'user': user.id, 'profile' : profile.id})
            elif not users and  usernameexitst:
                return Response({"Username exists, please change"},status=409)
            else:
                user=users[0]
                print(user)
                if user.check_password(request.data['password']):
                    token,created=Token.objects.get_or_create(user=user)

                    #return Response({'loggedin': 'An account exists with this email','exists': True,'token': token.key, 'user': user.id})
                return Response({'An account exists with this email, please login or request a password Reset Token on the Login Page'},status=409)
        except Exception as e:
            rollbar.report_exc_info(extra_data={'error': e, 'message': 'Error Creating Profile'})
            return Response({'error': 'Error Creating Profile' + str(e)}, status=409)


@api_view(['GET'])
@authentication_classes((SessionAuthentication, BasicAuthentication))
@permission_classes((IsAuthenticated,))
def example_view(request, format=None):
    content = {
            'user': str(request.user),  # `django.contrib.auth.User` instance.
            'auth': str(request.auth),  # None
        }
    return Response(content)



def userstoDJ(request):

    data = {}
    client = Cloudant('yadee', 'etxib991Tyeema01', account='yadee')
    userclient = Cloudant('yadee', 'etxib991Tyeema01', account='yadee')
    client.connect()
    userclient.connect()
    my_database = client[cloudantDB]
    usersdb = userclient[cloudantUsersDB]


    # FOR TRANSFERRING USERS FROM CLOUDANT TO YADEEBACKEND DJANGO
    for doc in usersdb:
        if 'isVendor' in doc.keys():
            if doc['isVendor'] == True:
                print('seller: '+doc['name'])
                try:
                    newseller = Seller.objects.create_user(username=doc['name'],
                                         email=doc['email'],
                                         password='temppass974694')
                    if 'phonenumber' in doc.keys():
                        newseller.phonenumber = doc['phonenumber']

                    if 'fullname' in doc.keys():
                        newseller.firstname = doc['fullname']
                    elif 'fullname' in doc.keys():
                        newseller.firstname = doc['fullname']

                    if 'bankname' in doc.keys():
                        newseller.save()
                        if (('accountnumber' in doc.keys()) & ('bankname' in doc.keys()) & ('accountname' in doc.keys() )):
                            x = Bank.objects.get_or_create(name=doc['bankname'])
                            BankDetails.objects.create(bankname = x[0], number= doc['accountnumber'], name=doc['accountname'], seller=newseller)
                    if 'address' in doc.keys():
                        Address.objects.create(line1= doc['address'], name='Home', phonenumber=doc['phonenumber'], seller=newseller)
                    if 'yedishopname' in doc.keys():
                        YediShop.objects.create( name= doc['yedishopname'], seller=newseller)
                    else:
                        YediShop.objects.create( name= newseller.username, seller=newseller)

                except IntegrityError:
                    pass

            else:
                print('shopper: '+doc['name'])
                try:
                    shopper = Shopper.objects.create_user(username=doc['name'],
                                         email=doc['email'],
                                         password='temppass974694')
                    if 'phonenumber' in doc.keys():
                        shopper.phonenumber = doc['phonenumber']
                    if 'fullname' in doc.keys():
                        shopper.firstname = doc['fullname']
                    if 'address' in doc.keys():
                        Address.objects.create(line1= doc['address'], name='Home', phonenumber=doc['phonenumber'], shopper=shopper)

                except IntegrityError:
                    pass


    return HttpResponse("at the users djing index")


            # newseller.firstname =
            # newseller.email = doc['email']
            # currprod = my_database[docid]
            # m = currprod.values()

def prodstoDJ(request):

    client = Cloudant('yadee', 'etxib991Tyeema01', account='yadee')
    userclient = Cloudant('yadee', 'etxib991Tyeema01', account='yadee')
    client.connect()
    userclient.connect()
    my_database = client[cloudantDB]

    for doc in my_database:
        if 'type' in doc.keys():
            if doc['type'] == 'productfile':
                    print('djing: ' + doc['_id'])
                    try:
                        newprod = Product()
                        newprod.cloudantid = doc['_id']
                        newprod.cloudantdoc = str(doc)
                        # print(parse(doc['dateadded']))
                        if 'dateadded' in doc.keys():
                            newprod.dateadded = parse(doc['dateadded'])
                            newprod.datemodified = parse(doc['datemodified'])
                        newprod.saleprice = doc['saleprice']
                        newprod.name = doc['productname']
                        if 'description' in doc.keys():
                            newprod.description = doc['description']
                        if 'desc' in doc.keys():
                            newprod.description = doc['desc']

                        newprod.seller = Seller.objects.get(username=doc['vendor'])
                        newprod.save()

                        #ATTRIBUTES
                        if 'options' in doc.keys():
                            print('...inoptions: '+ str(doc['options']))
                        if 'colors' in doc.keys():
                            a = Attribute.objects.get_or_create(name='colors', variety=doc['colors'])
                            newprod.attributes.add(a[0])
                        if 'sizes' in doc.keys():
                            s = Attribute.objects.get_or_create(name='sizes', variety=doc['sizes'])
                            newprod.attributes.add(s[0])

                        for cat in doc['categories']:
                            x = Category.objects.get_or_create(name=cat['name'])
                            newprod.categories.add(x[0])
                        newprod.isavailable = doc['isavailable']
                        newprod.isfeatured = doc['featured']
                        newprod.save()
                        for image in doc['images']:
                            img = Image.objects.create(oldurl=image['url'],name=newprod.name)
                            newprod.images.add(img)
                    except IntegrityError:
                        pass
    return HttpResponse("at the prods djing index")



    # for document in my_database:
    #     docid = document['_id']
    #     if docid.startswith('prod_artisan'):
    #         currprod = my_database[docid]
    #         m = currprod.values()
    #
    #         if 'wooed' not in currprod.keys():
    #             data['name'] = currprod['productname']
    #             data['regular_price'] = currprod['saleprice']
    #             attributes = []
    #
    #             if 'description' in currprod.keys():
    #                 data['description'] = currprod['description']
    #             if 'desc' in currprod.keys():
    #
    #                 data['desc'] = currprod['desc']
    #             data['categories'] = currprod['categories']
    #
    #             if 'dateadded' in currprod.keys():
    #                 data['date_created'] = currprod['dateadded']
    #
    #             if 'colors' in currprod.keys():
    #                 #data['colorsarr'] = currprod['colors']
    #                 attributes.append({})
    #                 #attributes[len(attributes)-1]['options'] = ast.literal_eval(currprod['colors'])
    #                 dd = currprod['colors']
    #                 attributes[len(attributes)-1]['options'] = currprod['colors'].split(',')
    #                 attributes[len(attributes)-1]['visible'] = True
    #                 attributes[len(attributes)-1]['position'] = 0
    #                 attributes[len(attributes)-1]['name'] = 'Color'
    #                 data['attributes'] = attributes
    #
    #             if 'sizes' in currprod.keys():
    #                 #data['colorsarr'] = currprod['colors']
    #                 attributes.append({})
    #                 attributes[len(attributes)-1]['options'] = currprod['sizes'].split(',')
    #                 #attributes[len(attributes)-1]['options'] = ast.literal_eval(currprod['sizes'])
    #                 attributes[len(attributes)-1]['visible'] = True
    #                 attributes[len(attributes)-1]['position'] = 0
    #                 attributes[len(attributes)-1]['name'] = 'Sizes'
    #                 data['attributes'] = attributes
    #
    #             data['featured'] = currprod['featured']
    #             data['sku'] = currprod['_id']
    #             images = []
    #             for currimage in currprod['images']:
    #                 images.append({})
    #                 images[counter]['src'] = currimage['url']
    #                 images[counter]['position'] = counter
    #                 counter = counter + 1
    #             counter = 0
    #             data['images'] = images
    #
    #             #print(currprod)
    #             print(wcapi.post("products", data).json())
    #             currprod['wooed'] = True
    #             currprod.save()

    return HttpResponse("at the djing index")


