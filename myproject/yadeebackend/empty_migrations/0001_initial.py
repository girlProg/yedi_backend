# Generated by Django 2.0.7 on 2018-08-12 23:03

from django.conf import settings
import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0009_alter_user_last_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(blank=True, default='', max_length=500)),
                ('line1', models.CharField(blank=True, default='', max_length=500)),
                ('line2', models.CharField(blank=True, default='', max_length=500)),
                ('state', models.CharField(blank=True, default='', max_length=500)),
                ('landmark', models.CharField(blank=True, default='', max_length=500)),
                ('phonenumber', models.CharField(blank=True, default='', max_length=500)),
            ],
            options={
                'ordering': ['-modified'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Attribute',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(blank=True, default='', max_length=500)),
                ('variety', models.CharField(blank=True, default='', max_length=500)),
            ],
            options={
                'ordering': ['-modified'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AttributeValue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('value', models.CharField(blank=True, default='', max_length=500, null=True)),
            ],
            options={
                'ordering': ['-modified'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Bank',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(blank=True, default='', max_length=500)),
            ],
            options={
                'ordering': ['-modified'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='BankDetails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(max_length=100, null=True)),
                ('number', models.CharField(max_length=11)),
                ('bankname', models.ForeignKey(blank=True, default='', null=True, on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.Bank')),
            ],
            options={
                'ordering': ['-modified'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CartItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('quantity', models.FloatField(default=0)),
                ('chosencolor', models.CharField(blank=True, default='', max_length=500)),
                ('chosensize', models.CharField(blank=True, default='', max_length=500)),
            ],
            options={
                'ordering': ['-modified'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(blank=True, default='', max_length=500)),
                ('catid', models.IntegerField(null=True)),
                ('parentid', models.IntegerField(default=0, null=True)),
            ],
            options={
                'ordering': ['-modified'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(blank=True, default='', max_length=500)),
                ('oldurl', models.CharField(blank=True, default='', max_length=500)),
                ('image', models.ImageField(blank=True, upload_to='Images/2018/ProductImages')),
            ],
            options={
                'ordering': ['-modified'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='LineItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('quantity', models.FloatField(default=0)),
                ('chosencolor', models.CharField(blank=True, default='', max_length=64)),
                ('chosensize', models.CharField(blank=True, default='', max_length=500)),
                ('chosenattribute', models.ForeignKey(blank=True, default='', on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.Attribute')),
            ],
            options={
                'ordering': ['-modified'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('_id', models.CharField(blank=True, default='', max_length=500)),
                ('status', models.CharField(choices=[('New', 'New'), ('Cancelled', 'Cancelled'), ('Completed', 'Completed'), ('Delivered', 'Delivered'), ('Dispatched', 'Dispatched'), ('Accepted', 'Accepted'), ('Declined', 'Declined'), ('Sent', 'Sent')], default='New', max_length=64)),
                ('comments', models.CharField(default='', max_length=500)),
                ('totalprice', models.FloatField(default=0)),
                ('shippingcost', models.FloatField(default=0)),
                ('sentemail', models.BooleanField(default=False)),
                ('address', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.Address')),
            ],
            options={
                'ordering': ['-modified'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('cloudantdoc', models.CharField(blank=True, default='', max_length=5000, null=True)),
                ('cloudantid', models.CharField(blank=True, default='', max_length=500)),
                ('name', models.CharField(blank=True, default='', max_length=500)),
                ('description', models.CharField(blank=True, default='', max_length=1000)),
                ('isavailable', models.BooleanField(default=True)),
                ('isactive', models.BooleanField(default=True)),
                ('isfeatured', models.BooleanField(default=True)),
                ('isonsale', models.BooleanField(default=False)),
                ('isdeleted', models.BooleanField(default=False)),
                ('price', models.FloatField(default=0)),
                ('discountprice', models.FloatField(default=0)),
                ('dateadded', models.DateTimeField(blank=True, null=True)),
                ('datemodified', models.DateTimeField(blank=True, null=True)),
                ('attributes', models.ManyToManyField(blank=True, to='yadeebackend.Attribute')),
                ('categories', models.ManyToManyField(blank=True, to='yadeebackend.Category')),
            ],
            options={
                'ordering': ['-modified'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ShippingMode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('mode', models.CharField(choices=[('Pick Up', 'Pick Up'), ('Delivery', 'Delivery')], default='Pick Up', max_length=500)),
            ],
            options={
                'ordering': ['-modified'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='YediShop',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(blank=True, default='', max_length=500)),
                ('isactive', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ['-modified'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='YediShopper',
            fields=[
                ('user_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('name', models.CharField(blank=True, default='', max_length=500)),
                ('phonenumber', models.CharField(blank=True, default='', max_length=500)),
            ],
            options={
                'verbose_name': 'Legacy Yedi Shopper',
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Seller',
            fields=[
                ('yedishopper_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='yadeebackend.YediShopper')),
                ('isactive', models.BooleanField(default=True)),
                ('isyediplus', models.BooleanField(default=False)),
                ('isVendor', models.BooleanField(default=True)),
            ],
            options={
                'verbose_name': 'Legacy Yedi Seller',
            },
            bases=('yadeebackend.yedishopper',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.AddField(
            model_name='order',
            name='customer',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.YediShopper'),
        ),
        migrations.AddField(
            model_name='order',
            name='shippingmode',
            field=models.ForeignKey(max_length=500, on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.ShippingMode'),
        ),
        migrations.AddField(
            model_name='lineitem',
            name='order',
            field=models.ForeignKey(blank=True, default='', on_delete=django.db.models.deletion.CASCADE, related_name='line_order', to='yadeebackend.Order'),
        ),
        migrations.AddField(
            model_name='lineitem',
            name='product',
            field=models.ForeignKey(blank=True, default='', on_delete=django.db.models.deletion.CASCADE, related_name='line_product', to='yadeebackend.Product'),
        ),
        migrations.AddField(
            model_name='image',
            name='product',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.Product'),
        ),
        migrations.AddField(
            model_name='attribute',
            name='values',
            field=models.ManyToManyField(blank=True, default='', to='yadeebackend.AttributeValue'),
        ),
        migrations.AddField(
            model_name='address',
            name='shopper',
            field=models.ForeignKey(blank=True, default='', null=True, on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.YediShopper'),
        ),
        migrations.AddField(
            model_name='yedishop',
            name='seller',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.Seller'),
        ),
        migrations.AddField(
            model_name='product',
            name='seller',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.Seller'),
        ),
        migrations.AddField(
            model_name='bankdetails',
            name='seller',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.Seller'),
        ),
        migrations.AddField(
            model_name='address',
            name='seller',
            field=models.ForeignKey(blank=True, default='', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='seller', to='yadeebackend.Seller'),
        ),
    ]
