from django.db import models
from django.contrib.auth.models import User
# try:
#     from myproject import settings
# except Exception:
#     from myproject import settings

# import settings
from django.conf import settings
import random
import string
import threading
from django.utils import timezone
from datetime import datetime, date, time
from django.template.loader import render_to_string
# from mptt.models import MPTTModel, TreeForeignKey
from django.utils.crypto import get_random_string
from django.utils.timezone import now

if settings.DEBUG == False:
    from django.contrib.postgres.fields import ArrayField


SMALL=64
MEDIUM=128
LARGE=256
MEGA=512

TRANSACTION_STATES=(('New','New'),('Accepted','Accepted'),('Declined','Declined'))
ORDER_STATES=(('New','New'),('Cancelled','Cancelled'),('Completed','Completed'),('Delivered','Delivered'),('Dispatched','Dispatched'),('Accepted','Accepted'),('Declined','Declined'),('Sent','Sent'))
SHIPPING_MODES=(('Pick Up','Pick Up'),('Delivery','Delivery'))
ATTRIBUTE_CHOOSE=(('Size','Size'),('Color','Color'))

letters=list(string.ascii_uppercase)+['AB','CD','EF','GH','JK','LM','MN','OP','PQ','RS','TU']
single_letters=list(string.ascii_uppercase)
numbers=[str(n) for n in range(10)]


class ParentModel(models.Model):
    created=models.DateTimeField(auto_now_add=True,null=True)
    modified=models.DateTimeField(auto_now=True,null=True)
    class Meta:
        abstract=True
        ordering = ['-modified']



class AvatarImage(ParentModel):
    image = models.ImageField(blank=True,upload_to='Images/2018/Avatars')



class UserProfile(ParentModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True, related_name='profile')
    avatar = models.OneToOneField(AvatarImage,on_delete=models.CASCADE, null=True, blank=True, related_name='profile')
    isSeller = models.BooleanField(default=False)
    pushtoken = models.CharField(blank=True,default='', max_length=500)
    phone = models.CharField(blank=True,default='', max_length=500)

    def __str__(self):
        return str(self.id) +' - '+self.user.username


class AnonymousShopper(ParentModel):
    deviceId = models.CharField(blank=True,default='', max_length=500)
    pushtoken = models.CharField(blank=True,default='', max_length=500)

    def __str__(self):
        return str(self.device)

class BaseUser(ParentModel):
    avatar = models.ImageField(blank=True,upload_to='Images/2018/Avatars')
    isSeller = models.BooleanField(default=True)
    class Meta:
        abstract=True


class Shopper(BaseUser):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True, related_name='shopper')
    name = models.CharField(blank=True,default='', max_length=500)
    phonenumber = models.CharField(blank=True,default='', max_length=500)

    class Meta:
        pass
        # verbose_name = "Legacy Yedi Shopper"

class Seller(BaseUser):
    id = models.BigIntegerField(primary_key = True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True, related_name='seller')
    isactive = models.BooleanField(default=True)
    isyediplus = models.BooleanField(default=False)
    class Meta:
        pass
        # verbose_name = "Legacy Yedi Seller"


class Address(ParentModel):
    name = models.CharField(blank=True,default='', max_length=500)
    line1 = models.CharField(blank=True,default='', max_length=500)
    line2 = models.CharField(blank=True,default='', max_length=500)
    state = models.CharField(blank=True,default='', max_length=500)
    landmark = models.CharField(blank=True,default='', max_length=500)
    phonenumber = models.CharField(blank=True,default='', max_length=500)
    user = models.ForeignKey(UserProfile, default='', blank=True, null=True, related_name='address', on_delete=models.CASCADE)


    def __str__(self):
        return self.line1

class Bank(ParentModel):
    name = models.CharField(blank=True,default='', max_length=500)

    def __str__(self):
        return self.name

class BankDetails(ParentModel):
    name =  models.CharField(blank=False, null=True, max_length=100)
    number = models.CharField(blank=False, max_length=11)
    bankname = models.ForeignKey(Bank,default='', null=True, blank=True, on_delete=models.CASCADE)
    profile = models.ForeignKey(UserProfile, default='', on_delete=models.CASCADE, related_name='bankdetails')

    def __str__(self):
        return self.name + ' - ' + self.number


class YediShop(ParentModel):
    logo = models.ImageField(blank=True,upload_to='Images/2018/Logo')
    name = models.CharField(blank=True,default='', max_length=500)
    isactive = models.BooleanField(default=True)
    seller = models.ForeignKey(UserProfile,  default='', null=True,on_delete=models.CASCADE, related_name='shop')

    def __str__(self):
        return self.name

# class Category(MPTTModel):
#     name = models.CharField(blank=True,default='', max_length=500)
#     parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
#     class MPTTMeta:
#         order_insertion_by = ['name']
#
#     def __str__(self):
#         return self.name


class Category(ParentModel):
    name = models.CharField(blank=True,default='', max_length=500)
    # catid = models.IntegerField(blank=False, null=True)
    slug = models.SlugField(default='', blank=True, null=True)
    image = models.ImageField(blank=True,upload_to='Images/2018/CategoryImages')
    parent = models.ForeignKey('self',blank=True, null=True ,related_name='children', on_delete=models.CASCADE)
    # parentid = models.IntegerField(default=0, null=True)
    class Meta:
        unique_together = ('slug', 'parent',)    #enforcing that there can not be two
        verbose_name_plural = "categories"

    def __str__(self):
        full_path = [self.name]                  # post.  use __unicode__ in place of
                                                 # __str__ if you are using python 2
        k = self.parent

        while k is not None:
            full_path.append(k.name)
            k = k.parent

        return ' -> '.join(full_path[::-1])



class AttributeType(ParentModel):
    name = models.CharField(blank=True,default='', null=True, max_length=MEDIUM)
    def __str__(self):
        return self.name

class AttributeValue(ParentModel):
    value = models.CharField(blank=True,default='', null=True, max_length=MEDIUM)
    def __str__(self):
        return self.value

class Attribute(ParentModel):
    name = models.CharField(blank=True, null=True, default='', choices=ATTRIBUTE_CHOOSE, max_length=SMALL)
    type = models.ForeignKey(AttributeType, related_name='attributes', default='', blank=True, max_length=500, on_delete=models.CASCADE)
    values = models.ManyToManyField(AttributeValue, default='', blank=True)
    variety = models.CharField(blank=True,default='', max_length=MEDIUM)

    def __str__(self):
        return str(self.type)







class Product(ParentModel):
    cloudantdoc = models.CharField(blank=True,null=True, default='', max_length=5000)
    cloudantid = models.CharField(blank=True,default='', max_length=500)
    name = models.CharField(blank=True,default='', max_length=500)
    description = models.CharField(blank=True,default='', max_length=1500)
    in_stock = models.BooleanField(default=True)
    isactive = models.BooleanField(default=True)
    isfeatured = models.BooleanField(default=True)
    on_sale = models.BooleanField(default=False)
    isdeleted = models.BooleanField(default=False)
    price = models.FloatField(default=0)
    discountprice = models.FloatField(default=0)
    categories = models.ManyToManyField(Category, blank=True, )
    dateadded = models.DateTimeField(blank=True, auto_now_add=False, null=True)
    datemodified = models.DateTimeField(blank=True, null=True)
    attributes = models.ManyToManyField(Attribute, blank=True, null=True )
    seller = models.ForeignKey(UserProfile, default='', on_delete=models.CASCADE)
    weight = models.FloatField(default=0.1)
    weightstr = models.CharField(blank=True,default='', max_length=SMALL)


    def __str__(self):
        return self.name + '-' + self.seller.user.username



class Image(ParentModel):
    name = models.CharField(blank=True,default='', max_length=500)
    oldurl = models.CharField(blank=True,default='', max_length=500)
    image = models.ImageField(blank=True,upload_to='Images/2018/ProductImages')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, blank=False, null=True, related_name="images")

class LogoImage(ParentModel):
    image = models.ImageField(blank=True,upload_to='Images/2018/Logos')
    shop = models.ForeignKey(YediShop, on_delete=models.CASCADE, blank=False, null=True, related_name="logoimage")


class CartItem(ParentModel):
    quantity = models.FloatField(default=0)
    chosencolor = models.CharField(blank=True,default='', max_length=500)
    chosensize = models.CharField(blank=True,default='', max_length=500)


class ShippingMode(ParentModel):
    mode = models.CharField(default='Pick Up', choices=SHIPPING_MODES,  max_length=500)
    def __str__(self):
        return self.mode

class Order(ParentModel):
    _id = models.CharField(blank=True,null=True, default='', max_length=500)
    address = models.ForeignKey(Address, on_delete=models.CASCADE, blank=True,null=True)
    customer = models.ForeignKey(UserProfile, blank=True, null=True, on_delete=models.CASCADE, related_name='orders')
    status =models.CharField(max_length=SMALL,default='New', null=True, choices=ORDER_STATES)
    comments = models.CharField(default='', max_length=500, blank=True, null=True,)
    shippingmode = models.ForeignKey(ShippingMode,  max_length=500, on_delete=models.CASCADE, blank=True,null=True,related_name='order')
    totalprice = models.FloatField(default=0, blank=True,null=True,)
    lineitemstotal = models.FloatField(default=0, blank=True,null=True,)
    shippingcost = models.FloatField(default=0, blank=True,null=True,)
    sentemail = models.BooleanField(default=False)


class PaymentMethod(ParentModel):
    name = models.CharField(blank=True,null=True, default='', max_length=SMALL)
    order = models.ForeignKey(Order, blank=True, null=True, on_delete=models.CASCADE, related_name='paymentmethod')


class LineItem(ParentModel):
    quantity = models.FloatField(default=0)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, blank=True, default='',related_name='line_product')
    order = models.ForeignKey(Order, on_delete=models.CASCADE, blank=True, default='',related_name='line_items')
    chosenattribute = models.ForeignKey(Attribute, on_delete=models.CASCADE, null=True, blank=True, default='')
    chosencolor = models.CharField(blank=True, null=True, default='', max_length=SMALL)
    chosensize = models.CharField(blank=True, null=True, default='', max_length=500)

    def __str__(self):
        if self.order._id:
            return self.product.name + '-' + self.order._id
        else:
            return self.product.name


    # def total(self):
    #     total=sum([self.products.quantity for suborder in self.objects.suborder.all()])
    #     return min(0,self.quantity-total)

    # def order_number(customer):
    #     O=Order.objects.filter(created__gte=datetime.combine(date.today(),time()),customer=customer)
    #     return random.choice(single_letters)+letters[date.today().month-1]+letters[date.today().day-1]+str(O.count()+1)


class Transaction(ParentModel):
    _id = models.CharField(blank=True,null=True, default='', max_length=500)
    line_item = models.ForeignKey(LineItem, blank=True, null=True, on_delete=models.CASCADE, related_name='transaction')
    status =models.CharField(max_length=SMALL,default='New', null=True, choices=TRANSACTION_STATES)

class ResetCodePack(ParentModel):
    resetcode = models.CharField(blank=True,null=True, default='', max_length=500)
    email =models.CharField(max_length=MEDIUM,default='', null=True)

class CouponCode(ParentModel):
    code = models.CharField(blank=True,null=True, default='', max_length=500)
    amount =models.CharField(max_length=MEDIUM,default='', null=True)
    discount_type =models.CharField(max_length=MEDIUM,default='', null=True)
    seller =models.ForeignKey(UserProfile,  on_delete=models.CASCADE, blank=True, null=True, default='',related_name='coupon')
    date_expires = models.DateField('Expiry Date', default=now)

class RegisterEvent(ParentModel):
    seller = models.ForeignKey(UserProfile,  default='', null=True,on_delete=models.CASCADE, related_name='events')
    phone = models.CharField(blank=True,null=True, default='', max_length=500)
    businessName = models.CharField(blank=True,null=True, default='', max_length=500)
    category = models.CharField(blank=True,null=True, default='', max_length=500)
    isRegular = models.BooleanField(default=False)
    gotTable = models.BooleanField(default=False)
    hasPaid = models.BooleanField(default=False)
