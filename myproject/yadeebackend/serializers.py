from rest_framework import serializers
from .models import Product, LineItem, PaymentMethod, AnonymousShopper, CouponCode, RegisterEvent, ResetCodePack, ShippingMode, AvatarImage, Bank, BankDetails, Transaction, LogoImage, UserProfile, Category, Image, AttributeType,  Address, Seller, Attribute, User, Shopper, YediShop, Order, AttributeValue
from django.contrib.auth.validators import UnicodeUsernameValidator, ASCIIUsernameValidator
from .mailchimp_email import send_workflow_email




class SellerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Seller
        fields = "__all__"

class AvatarImageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AvatarImage
        fields = "__all__"

class BankSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bank
        fields = "__all__"

class BankDetailsSerializer(serializers.ModelSerializer):
    bankname = BankSerializer
    class Meta:
        model = BankDetails
        fields = ('bankname', )


class YediShopperSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Shopper
        fields= "__all__"
        depth = 2
        extra_kwargs = {
            'username': {
            'validators': [ASCIIUsernameValidator()],
            }
        }


class YediShopSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = YediShop
        fields = ('name',  'seller', 'isactive')



class AddressSerializer(serializers.HyperlinkedModelSerializer):
    # seller=SellerSerializer()
    # shopper = YediShopperSerializer()
    class Meta:
        model = Address
        # fields = '__all__'
        fields = ('name', 'line1', 'line2', 'state', 'landmark', 'phonenumber', 'user', )



class UserProfileSerializer(serializers.ModelSerializer):
    shop = YediShopSerializer
    avatar = AvatarImageSerializer
    address = AddressSerializer
    # bankdetails = BankDetailsSerializer
    class Meta:
        model = UserProfile
        fields = ('id', 'pushtoken', 'avatar', 'shop', 'user', 'address', 'bankdetails')
        depth = 2

class UserSerializer(serializers.HyperlinkedModelSerializer):
    shopper = YediShopperSerializer
    seller = SellerSerializer
    profile = UserProfileSerializer
    class Meta:
        model = User
        fields = ('id','username','first_name','last_name','email', 'profile')
        depth = 2



class ImageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Image
        fields = ('name', 'image', 'url', 'oldurl')


class AttributeTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AttributeType
        fields  = ('name', )

class AttributeValueSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AttributeValue
        fields  = ('value', )


class AttributeSerializer(serializers.HyperlinkedModelSerializer):
    type  =  AttributeTypeSerializer
    class Meta:
        model = Attribute
        fields = ('id','type', 'values',)
        depth = 2


class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        # fields = ('id', 'name','image',)
        fields = '__all__'

class CategorySerializer(serializers.ModelSerializer):
    parent = SubCategorySerializer
    class Meta:
        model = Category
        # fields = ('id', 'name', 'parent','image')
        fields = '__all__'
        depth = 1



class ProductSerializer(serializers.HyperlinkedModelSerializer):
    seller = UserProfileSerializer()
    attributes = AttributeSerializer(many=True)
    categories = CategorySerializer(many=True)
    images = ImageSerializer(many=True, required=True)
    class Meta:
        model = Product
        # fields = '__all__'
        fields = ('id', 'seller', 'description', 'name', 'attributes','isactive', 'isfeatured','on_sale' ,'isdeleted', 'price' ,'discountprice', 'categories', 'dateadded' ,'datemodified','images', 'in_stock')
        depth = 2




class ShippingModeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShippingMode
        fields = "__all__"



class LineItemSerializer(serializers.ModelSerializer):
    product = ProductSerializer
    class Meta:
        model = LineItem
        fields = ('quantity', 'product', 'order')
        # fields  = '__all__'
        depth = 3


class OrderSerializer(serializers.ModelSerializer):
    shippingmode = ShippingModeSerializer
    address = AddressSerializer
    line_items = LineItemSerializer(many=True)

    class Meta:
        model = Order
        fields = "__all__"
        # fields = ('id', 'paymentmethod','line_items', 'created', 'address', 'customer', 'status', 'comments', 'shippingmode',  'totalprice', 'shippingcost', 'sentemail')
        depth = 2

class PaymentMethodSerializer(serializers.HyperlinkedModelSerializer):
    order =  OrderSerializer
    class Meta:
        model = PaymentMethod
        fields = "__all__"


class TransactionSerializer(serializers.ModelSerializer):
    line_item = LineItemSerializer
    class Meta:
        model = Transaction
        fields = "__all__"
        depth = 3
        # fields = ('status', 'lineitem', '_id')


class ResetCodePackSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResetCodePack
        fields = "__all__"


class CouponCodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CouponCode
        fields = "__all__"


class RegisterEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegisterEvent
        fields = "__all__"

    def create(self, validated_data):
        obj, created = RegisterEvent.objects.get_or_create(**validated_data)
        try:
            if created:
                send_workflow_email(obj.seller.user.email, obj.seller.user.first_name)
        except Exception as e:
            pass
        return obj


class AnonymousShopperSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnonymousShopper
        fields = "__all__"

    def create(self, validated_data):
        try:
            profiles = UserProfile.objects.filter(pushtoken=validated_data['pushtoken'])
            if len(profiles) == 0:
                obj, created = AnonymousShopper.objects.get_or_create(**validated_data)
                return obj
            return profiles[0]
        except Exception as e:
            pass




class CreateUserSerializer(serializers.Serializer):
    username = serializers.EmailField(required=True)
    first_name= serializers.CharField(max_length=30,required=True)
    last_name= serializers.CharField(max_length=150,required=True)
    password = serializers.CharField(max_length=200)

class LoginUserSerializer(serializers.Serializer):
    username = serializers.EmailField(required=True)
    password = serializers.CharField(max_length=200)

