from rest_framework import permissions
from .models import UserProfile, User


class IsCustomer(permissions.BasePermission):
    def has_permission(self, request, view):
        return UserProfile.objects.filter(user=request.user).count() > 0


class IsStaff(permissions.BasePermission):
    def has_permission(self, request, view):
        return User.objects.filter(user=request.user).count()[0].isStaff


class IsShopper(permissions.BasePermission):
    def has_permission(self, request, view):
        return User.objects.filter(user=request.user).count()[0].isStaff

