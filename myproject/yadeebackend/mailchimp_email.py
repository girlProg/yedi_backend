from mailchimp3 import MailChimp


def send_workflow_email(email, profile):
    client = MailChimp(mc_api='db3c95fa34747566c3c5178dc606d9d3-us20', mc_user='tymah')


    #alls = client.lists.all(get_all=True, fields="lists.name,lists.id")


    # add John Doe with email john.doe@example.com to list matching id '123456'

    #https://us20.api.mailchimp.com/3.0/automations/fcd8d0fbfd/emails/3e1d64e68f/queue - WELCOME MESSAGE CAMPIGN
    #https://us20.api.mailchimp.com/3.0/automations/c508ead6b0/emails/a182693646/queue - ATS CAMPAGN

    # alls = client.automations.get(workflow_id='fcd8d0fbfd') - 1f700f5e46 listid
    # alls = client.automations.get(workflow_id='c508ead6b0') - 7af02743d8 listid

    client.lists.members.create('7af02743d8', {
        'email_address': email,
        'status': 'subscribed',
        'merge_fields': {
            'FNAME': profile,
            'LNAME': '',
        },
    })
    alls = client.automations.emails.queues.create(workflow_id='c508ead6b0',
                                                   email_id='a182693646',
                                                   data={'email_address': email,
                                                        'status': 'subscribed',
                                                        'merge_fields': {
                                                            'FNAME': profile,
                                                            'LNAME': 'Doe',
                                                        },})


    print(alls)
    return alls

def moveusers():
    client = MailChimp(mc_api='db3c95fa34747566c3c5178dc606d9d3-us20', mc_user='tymah')


    #alls = client.lists.all(get_all=True, fields="lists.name,lists.id")


    # add John Doe with email john.doe@example.com to list matching id '123456'

    #https://us20.api.mailchimp.com/3.0/automations/fcd8d0fbfd/emails/3e1d64e68f/queue

    # alls = client.automations.get(workflow_id='fcd8d0fbfd')
    client.lists.members.create('1f700f5e46', {
        'email_address': 'commissions@yedi.com.ng',
        'status': 'subscribed',
        'merge_fields': {
            'FNAME': 'John',
            'LNAME': 'Doe',
        },
    })
    alls = client.automations.emails.queues.create(workflow_id='fcd8d0fbfd',
                                                   email_id='3e1d64e68f',
                                                   data={'email_address': 'commissions@yedi.com.ng',
                                                        'status': 'subscribed',
                                                        'merge_fields': {
                                                            'FNAME': 'John',
                                                            'LNAME': 'Doe',
                                                        },})

    # alls = client.lists.members.create('1f700f5e46', {
    #     'email_address': 'commissions@yedi.com.ng',
    #     'status': 'subscribed',
    #     'merge_fields': {
    #         'FNAME': 'John',
    #         'LNAME': 'Doe',
    #     },
    # })

    print(alls)
    return alls
