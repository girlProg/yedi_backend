
from django.core.mail import send_mail

from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
import rollbar

import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def send_resetcode_email(resetcode, email):
    try:
        # msg = MIMEMultipart('alternative')
        # msg['Subject'] = "Your Password Reset Request"
        # msg['From'] = 'hello.yedi@gmail.com'
        # msg['To'] = email
        # msg['Bcc'] = 'hello@yedi.com.ng'
        #
        # text = "Hi!\nHow are you?\nHere is the link you wanted:\nhttps://www.python.org"
        # tcontext = { 'resetcode': resetcode, 'email': email } # provide your context here, if applicable
        # html = render_to_string('./yadeebackend/templates/passwordresetemail.html', tcontext)
        #
        # part1 = MIMEText(text, 'plain')
        # part2 = MIMEText(html, 'html')
        # msg.attach(part1)
        # msg.attach(part2)
        #
        # s = smtplib.SMTP('in-v3.mailjet.com', 587)
        # s.login('fff5db6cba766d5dfd13bc866b085b07', 'b4bd638f0bef020a50e755675e95a6df')
        # s.sendmail(msg['From'], msg['To'], msg.as_string())
        # s.quit()

        tsubject = 'Your Password Reset Request'
        tcontext = { 'resetcode': resetcode, 'email': email } # provide your context here, if applicable
        tmessage = render_to_string('./yadeebackend/templates/passwordresetemail.html', tcontext)

        temail = EmailMultiAlternatives(tsubject, tmessage, 'Yedi App <postmaster@yedi.com.ng>', [email,], bcc=['hello.yedi@gmail.com'])
        temail.attach_alternative(tmessage, 'text/html') # crucial part
        temail.send()
    except Exception as e:
        rollbar.report_exc_info(extra_data={'error': e, 'message': 'couldnt send transaction email to: ' +email})

def send_passwordchangedone_email(email, username):
    try:

        tsubject = 'Password Changed'
        tcontext = { 'email': email, 'username': username } # provide your context here, if applicable
        tmessage = render_to_string('./yadeebackend/templates/passwordreset_done.html', tcontext)

        temail = EmailMultiAlternatives(tsubject, tmessage, 'Yedi App <postmaster@yedi.com.ng>', [email,], bcc=['hello.yedi@gmail.com'])
        temail.attach_alternative(tmessage, 'text/html') # crucial part
        temail.send()
    except Exception as e:
        rollbar.report_exc_info(extra_data={'error': e, 'message': 'couldnt send transaction email to: ' +email})


def send_tranaction_email(neworder, li, sellerprofile):
    try:
        tsubject = 'Your Yedi Transaction'
        tcontext = { 'neworder': neworder, 'orderid': neworder._id, 'product': li.product, 'lineitem': li, 'id' : li.product.id, 'seller': sellerprofile, 'imageurl': li.product.images.all()[0].image } # provide your context here, if applicable
        tmessage = render_to_string('./yadeebackend/templates/sellerTransactionNotification.html', tcontext)

        temail = EmailMultiAlternatives(tsubject, tmessage, 'Yedi Transactions<trans@yedi.com.ng>', [sellerprofile.email,], bcc=['hello.yedi@gmail.com'])
        temail.attach_alternative(tmessage, 'text/html') # crucial part
        temail.send()
    except Exception as e:
        rollbar.report_exc_info(extra_data={'error': e, 'message': 'couldnt send transaction email to: ' +sellerprofile.email})


def send_new_order_email(neworder):
    try:
        tsubject = 'Order Placed'
        tcontext = { 'neworder': neworder, 'orderid': neworder._id, 'lineitems' : neworder.line_items.all() } # provide your context here, if applicable
        tmessage = render_to_string('./yadeebackend/templates/newOrderNotification.html', tcontext)

        temail = EmailMultiAlternatives(tsubject, tmessage, 'Yedi Orders<orders@yedi.com.ng>', [neworder.customer.user.email,], bcc=['hello.yedi@gmail.com'])
        temail.attach_alternative(tmessage, 'text/html') # crucial part
        temail.send()
    except Exception as e:
        rollbar.report_exc_info(extra_data={'error': e, 'message': 'couldnt send order email to: ' +neworder.customer.user.email})




def send_signup_email(user, profile):
    try:
        tsubject = 'Welcome to Yedi'
        tcontext = { 'user': user, 'profile': profile } # provide your context here, if applicable
        tmessage = render_to_string('./yadeebackend/templates/welcometoyedi.html', tcontext)

        temail = EmailMultiAlternatives(tsubject, tmessage, 'Yedi App<hello@yedi.com.ng>', [user.email,], bcc=['hello.yedi@gmail.com'])
        temail.attach_alternative(tmessage, 'text/html') # crucial part
        temail.send()
    except Exception as e:
        rollbar.report_exc_info(extra_data={'error': e, 'message': 'couldnt send signup email to: ' +user.email})




def send_welcomeback_email(user, profile):
    try:
        tsubject = 'Welcome to Yedi 2.0'
        tcontext = { 'user': user, 'profile': profile } # provide your context here, if applicable
        tmessage = render_to_string('./yadeebackend/templates/yedi2.html', tcontext)

        temail = EmailMultiAlternatives(tsubject, tmessage, 'Yedi App<hello@yedi.com.ng>', [user.email,], bcc=['hello.yedi@gmail.com'])
        temail.attach_alternative(tmessage, 'text/html') # crucial part
        temail.send()
    except Exception as e:
        rollbar.report_exc_info(extra_data={'error': e, 'message': 'couldnt send welcomeback email to: ' +user.email})


