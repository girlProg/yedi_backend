# Generated by Django 2.0.7 on 2018-08-18 23:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('yadeebackend', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='shopper',
            name='avatar',
            field=models.ImageField(blank=True, upload_to='Images/2018/Avatars'),
        ),
        migrations.AddField(
            model_name='yedishop',
            name='logo',
            field=models.ImageField(blank=True, upload_to='Images/2018/Logo'),
        ),
    ]
