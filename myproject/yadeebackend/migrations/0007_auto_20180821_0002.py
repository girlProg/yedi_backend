# Generated by Django 2.0.7 on 2018-08-21 00:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('yadeebackend', '0006_auto_20180820_2353'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='isavailable',
            new_name='in_stock',
        ),
    ]
