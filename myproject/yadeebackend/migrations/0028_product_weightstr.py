# Generated by Django 2.1.1 on 2018-10-07 01:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('yadeebackend', '0027_auto_20181007_0120'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='weightstr',
            field=models.CharField(blank=True, default='', max_length=64),
        ),
    ]
