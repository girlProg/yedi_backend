# Generated by Django 2.0.7 on 2018-08-21 01:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('yadeebackend', '0009_remove_attribute_type'),
    ]

    operations = [
        migrations.CreateModel(
            name='AttributeType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(blank=True, default='', max_length=128, null=True)),
            ],
            options={
                'ordering': ['-modified'],
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='attribute',
            name='name',
        ),
        migrations.AlterField(
            model_name='attribute',
            name='variety',
            field=models.CharField(blank=True, default='', max_length=128),
        ),
        migrations.AlterField(
            model_name='attributevalue',
            name='value',
            field=models.CharField(blank=True, default='', max_length=128, null=True),
        ),
    ]
