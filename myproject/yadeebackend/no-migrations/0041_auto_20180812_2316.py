# Generated by Django 2.0.7 on 2018-08-12 23:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('yadeebackend', '0040_auto_20180806_0128'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='_id',
            field=models.CharField(blank=True, default='', max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='address',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.Address'),
        ),
        migrations.AlterField(
            model_name='order',
            name='comments',
            field=models.CharField(blank=True, default='', max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='customer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.YediShopper'),
        ),
        migrations.AlterField(
            model_name='order',
            name='shippingcost',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='shippingmode',
            field=models.ForeignKey(blank=True, max_length=500, null=True, on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.ShippingMode'),
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(choices=[('New', 'New'), ('Cancelled', 'Cancelled'), ('Completed', 'Completed'), ('Delivered', 'Delivered'), ('Dispatched', 'Dispatched'), ('Accepted', 'Accepted'), ('Declined', 'Declined'), ('Sent', 'Sent')], default='New', max_length=64, null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='totalprice',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
    ]
