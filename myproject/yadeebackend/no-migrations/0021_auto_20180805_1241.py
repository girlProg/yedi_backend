# Generated by Django 2.0.7 on 2018-08-05 12:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('yadeebackend', '0020_auto_20180805_1142'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='saleprice',
            new_name='price',
        ),
        migrations.RemoveField(
            model_name='order',
            name='dateadded',
        ),
        migrations.AddField(
            model_name='lineitem',
            name='product',
            field=models.ManyToManyField(related_name='line_product', to='yadeebackend.Product'),
        ),
        migrations.AlterField(
            model_name='order',
            name='shippingmode',
            field=models.ForeignKey(max_length=500, on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.ShippingMode'),
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(choices=[('New', 'New'), ('Cancelled', 'Cancelled'), ('Completed', 'Completed'), ('Delivered', 'Delivered'), ('Dispatched', 'Dispatched'), ('Accepted', 'Accepted'), ('Declined', 'Declined'), ('Sent', 'Sent')], default='New', max_length=64),
        ),
        migrations.AlterField(
            model_name='shippingmode',
            name='mode',
            field=models.CharField(choices=[('Pick Up', 'Pick Up'), ('Delivery', 'Delivery')], default='Pick Up', max_length=500),
        ),
    ]
