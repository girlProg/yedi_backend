# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-12-04 22:18
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('yadeebackend', '0003_auto_20171203_2312'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='isavailable',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='bankdetails',
            name='bankname',
            field=models.ForeignKey(blank=True, default='', null=True, on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.Bank'),
        ),
    ]
