# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-12-03 23:05
from __future__ import unicode_literals

from django.conf import settings
import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default='', max_length=500)),
                ('line1', models.CharField(blank=True, default='', max_length=500)),
                ('line2', models.CharField(blank=True, default='', max_length=500)),
                ('state', models.CharField(blank=True, default='', max_length=500)),
                ('landmark', models.CharField(blank=True, default='', max_length=500)),
                ('phonenumber', models.CharField(blank=True, default='', max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='Attribute',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default='', max_length=500)),
                ('variety', models.CharField(blank=True, default='', max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='Bank',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default='', max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='BankDetails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('number', models.CharField(max_length=11)),
                ('bankname', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.Bank')),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default='', max_length=500)),
                ('catid', models.IntegerField()),
                ('parentid', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default='', max_length=500)),
                ('image', models.ImageField(upload_to='Images/ProductImages')),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('_id', models.CharField(blank=True, default='', max_length=500)),
                ('dateadded', models.DateField(blank=True)),
                ('status', models.CharField(default='', max_length=500)),
                ('comments', models.CharField(default='', max_length=500)),
                ('shippingmode', models.CharField(default='', max_length=500)),
                ('totalprice', models.FloatField(default=0)),
                ('shippingcost', models.FloatField(default=0)),
                ('sentemail', models.BooleanField(default=False)),
                ('address', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.Address')),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default='', max_length=500)),
                ('isactive', models.BooleanField(default=True)),
                ('isfeatured', models.BooleanField(default=True)),
                ('isonsale', models.BooleanField(default=False)),
                ('isdeleted', models.BooleanField(default=False)),
                ('saleprice', models.FloatField(default=0)),
                ('discountprice', models.FloatField(default=0)),
                ('dateadded', models.DateField(blank=True)),
                ('datemodified', models.DateField(blank=True)),
                ('qty', models.IntegerField(default=0)),
                ('attributes', models.ManyToManyField(to='yadeebackend.Attribute')),
                ('categories', models.ManyToManyField(to='yadeebackend.Category')),
            ],
        ),
        migrations.CreateModel(
            name='ShippingMode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mode', models.CharField(default='Pick Up', max_length=500)),
                ('address', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.Address')),
            ],
        ),
        migrations.CreateModel(
            name='YediShop',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default='', max_length=500)),
                ('isactive', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='YediShopper',
            fields=[
                ('user_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('name', models.CharField(blank=True, default='', max_length=500)),
                ('phonenumber', models.CharField(blank=True, default='', max_length=500)),
            ],
            options={
                'abstract': False,
                'verbose_name_plural': 'users',
                'verbose_name': 'user',
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Seller',
            fields=[
                ('yedishopper_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='yadeebackend.YediShopper')),
                ('isactive', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
                'verbose_name_plural': 'users',
                'verbose_name': 'user',
            },
            bases=('yadeebackend.yedishopper',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.AddField(
            model_name='order',
            name='customer',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.YediShopper'),
        ),
        migrations.AddField(
            model_name='order',
            name='products',
            field=models.ManyToManyField(to='yadeebackend.Product'),
        ),
        migrations.AddField(
            model_name='image',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.Product'),
        ),
        migrations.AddField(
            model_name='address',
            name='shopper',
            field=models.ForeignKey(blank=True, default='', on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.YediShopper'),
        ),
        migrations.AddField(
            model_name='yedishop',
            name='seller',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.Seller'),
        ),
        migrations.AddField(
            model_name='product',
            name='seller',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.Seller'),
        ),
        migrations.AddField(
            model_name='bankdetails',
            name='seller',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='yadeebackend.Seller'),
        ),
        migrations.AddField(
            model_name='address',
            name='seller',
            field=models.ForeignKey(blank=True, default='', on_delete=django.db.models.deletion.CASCADE, related_name='seller', to='yadeebackend.Seller'),
        ),
    ]
