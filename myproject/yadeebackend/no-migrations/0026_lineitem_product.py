# Generated by Django 2.0.7 on 2018-08-05 19:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('yadeebackend', '0025_auto_20180805_1944'),
    ]

    operations = [
        migrations.AddField(
            model_name='lineitem',
            name='product',
            field=models.ForeignKey(blank=True, default='', on_delete=django.db.models.deletion.CASCADE, related_name='line_product', to='yadeebackend.Product'),
        ),
    ]
