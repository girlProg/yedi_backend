from django.apps import AppConfig


class YadeebackendConfig(AppConfig):
    name = 'yadeebackend'
    verbose_name = 'Server'
