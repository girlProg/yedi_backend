from django.contrib import admin
from .models import Product, Category, Attribute, AttributeValue, AttributeType, ShippingMode, Image, Address, Seller, Order, Shopper, YediShop,Bank, BankDetails, User, LineItem
from reversion.admin import VersionAdmin
# from mptt.admin import MPTTModelAdmin
import inspect
from django.db import models as django_models
try:
    from myproject.yadeebackend import models
except Exception:
    from . import models
    pass
# Register your models here.

#
# admin.site.register(Image)
# admin.site.register(Seller)
# admin.site.register(Category)
# admin.site.register(Shopper)
# admin.site.register(YediShop)
# admin.site.register(Address)
# admin.site.register(BankDetails)
# admin.site.register(Bank)
# admin.site.register(LineItem)
# admin.site.register(ShippingMode)
# admin.site.register(Attribute)
# admin.site.register(AttributeValue)
#
#
# class ImageInline(admin.StackedInline):
#     model = Image
#     extra = 1
# #
# class AddressInline(admin.StackedInline):
#     model = Address
#     extra = 1
#
# class YediShopInline(admin.StackedInline):
#     model = YediShop
#     extra = 1
#
# class ProductInline(admin.StackedInline):
#     model = Product
#     extra = 1
# #
# class BankDetailsInline(admin.StackedInline):
#     model = BankDetails
#     extra = 1
#
# class LineItemInline(admin.StackedInline):
#     model = LineItem
#     extra = 1
# #
# # class SellerInline(admin.StackedInline):
# #     model = Seller
# #     extra = 1
#
# class OrderAdmin(VersionAdmin):
#     fieldsets = [
#         (None,{'fields': ['address','customer', 'status', 'comments','shippingmode', 'shippingcost', 'totalprice']
#         })
#         ,
#     ]
#     search_fields = ['name','customer__first_name', 'status', 'totalprice']
#
#     inlines = [ LineItemInline ]
#
# class ProductAdmin(VersionAdmin):
#     fieldsets = [
#         (None,{'fields': ['cloudantid','name',('isactive', 'isfeatured' ,'isdeleted'), 'price' ,'description','discountprice', 'categories', 'attributes',('dateadded' ,'datemodified'), 'seller']
#         })
#         ,
#     ]
#     search_fields = ['name', 'cloudantid', 'seller__name', 'seller__email', 'seller__phonenumber']
#
#     inlines = [ ImageInline]
#
# class YediShopperAdmin(VersionAdmin):
#     fieldsets = [
#         (None,{'fields': ['name', 'phonenumber' ,'username', 'email', 'firstname']
#         })
#         ,
#     ]
#     inlines = [ AddressInline, ]
#
# class SellerAdmin(VersionAdmin):
#     fieldsets = [
#         (None,{'fields': [ 'isactive']
#         })
#         ,
#     ]
#     inlines = [ AddressInline, YediShopInline, BankDetailsInline, ProductInline]
#
#     search_fields = ['user__username', 'phonenumber', 'user__email', 'user__first_name']
#
# class YediShopAdmin(VersionAdmin):
#
#     fieldsets = [
#         (None,{'fields': ['name', 'seller' ,'isactive',]
#         })
#         ,
#     ]
#     inlines = [ ProductInline]


    # list_display = ('number', 'tenant')
    #
    # def save_model(self, request, obj, form, change):
    #     if (isinstance(obj.seller, Seller) ):
    #         super(ProductAdmin, self).save_model(request, obj, form, change)
    #     elif (isinstance(obj.seller, User)):
    #         x = Seller()
    #         x.username = request.user.username
    #         x.email = request.user.email
    #         obj.seller = x
    #         super(ProductAdmin, self).save_model(request, obj, form, change)

# #
# admin.site.register(Product, ProductAdmin)
# admin.site.register(Order, OrderAdmin)


for name,obj in inspect.getmembers(models):
    if inspect.isclass(obj) and issubclass(obj,django_models.Model) and obj._meta.abstract is not True and name not in ['User']:
        class Admin(admin.ModelAdmin):
            def get_list_display(self,request):
                return [t.name for t in self.model._meta.fields if t.editable]
            class Meta:
                model = obj
        admin.site.register(obj,Admin)