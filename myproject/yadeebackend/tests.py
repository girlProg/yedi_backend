from django.test import TestCase
from rest_framework.test import APIRequestFactory

# Create a JSON POST request
factory = APIRequestFactory()
request = factory.post('/notes/', {'title': 'new idea'}, format='json')