# try:
#     from myproject import settings
# except Exception:
#     from myproject import settings

from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from rest_framework import routers
router = routers.DefaultRouter()


from . import views

router.register(r'users', views.UserViewSet)
router.register(r'products', views.ProductViewSet, base_name='product')
router.register(r'categories', views.CategoryViewSet)
router.register(r'transactions', views.TransactionViewSet, base_name='transactions')
router.register(r'profiles', views.UserProfileViewSet)
router.register(r'sellers', views.SellerViewSet)
router.register(r'yedishops', views.YediShopViewSet)
router.register(r'shoppers', views.YediShopperViewSet)
router.register(r'orders', views.OrderViewSet, base_name='order')
router.register(r'lineitems', views.LineItemViewSet, base_name='lineitems')
router.register(r'addresses', views.AddressViewSet)
router.register(r'images', views.ImageViewSet, base_name='image')
router.register(r'attributes', views.AttributeViewSet)
router.register(r'attributevalues', views.AttributeValueViewSet)
router.register(r'avatars', views.AvatarImageViewSet)
router.register(r'attributetypes', views.AttributeTypeViewSet)
router.register(r'resetcode', views.ResetCodePackViewSet)
router.register(r'couponcode', views.CouponCodeViewSet)
router.register(r'registerevent', views.RegisterEventViewSet)
router.register(r'anonshopper', views.AnonymousShopperViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^userstodj', views.userstoDJ, name='userstodj'),
    url(r'^uploader', views.post_uploadimg, name='uploader'),
    url(r'^prodstodj', views.prodstoDJ, name='prodstoDJ'),
    url(r'^login', views.example_view, name='login'),
    url('user/create', views.CreateUserView.as_view()),
    url('user/login', views.LoginUserView.as_view()),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)