from cloudant.client import Cloudant
from django_cron import CronJobBase, Schedule
from django.core.mail import send_mail
import urllib.request
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from datetime import datetime


def my_scheduled_job():
    print('**************************************************************')
    print('cron job running')
    client = Cloudant('yadee', 'Tyeema01', account='yadee')
    client.connect()
    my_database = client['yadee_test']
    users_database = client['_users']
    textusr = 'tymah'
    textpwd = 'tyeema'
    sender = 'YediOrder'
    tsender = 'YediTrans'
    transSMSmsg= 'You+have+a+new+transaction+in+Yedi.+Goto+"Account+>+My+Transactions"+in+Yedi+to+change+transaction+status'
    orderSMSmsg= "Thank+you+for+Ordering+from+Yedi.+You+can+track+progress+in+'My+Orders'."

    # Iterate over a "continuous" _db_updates feed with additional options
    changes = my_database.changes(feed='longpoll',  heartbeat=100, descending=False)
    for change in changes:
        if (change['id'].startswith('oc_') | change['id'].startswith('op_')):
            if(change['changes'][0]['rev'].startswith('1-')):
                neworder = my_database[change['id']]
                print( neworder['_id'])
                print('New order for: '+ neworder['user']['name'])
                #for product in neworder['products']:
                #    product['dataimage'] = "data:image/jpeg;base64,"+product['image']
                    #email & text to buyer

                subject = 'Your Yedi Order'
                context = { 'neworder': neworder, 'id' : neworder['_id'] } # provide your context here, if applicable
                message = render_to_string('invoice.html', context)

                email = EmailMultiAlternatives(subject, message, 'orders@yedi.com.ng', [neworder['user']['email']], bcc=['hello.yedi@gmail.com'])
                email.attach_alternative(message, 'text/html') # crucial part
                email.send()
                print('Buyer Emailed: '+ neworder['user']['email'])

                xx = urllib.request.urlopen("http://smsclone.com/index.php?option=com_spc&comm=spc_api&username="+textusr+"&password="+textpwd+"&sender="+sender+"&recipient="+neworder['user']['phonenumber']+"&message="+orderSMSmsg+"").read()
                print ('Buyer SMSed with cost: '+str(xx))

                #email and text to sellers
                for product in neworder['products']:
                    #product['image'] = 'data:image/jpeg;base64,'+ product['image']

                    userprefix = 'org.couchdb.user:'+ product['vendor']
                    sellerprofile = users_database[userprefix]

                    tsubject = 'Your Yedi Transaction'
                    tcontext = { 'orderid': neworder['_id'], 'product': product, 'id' : product['_id'], 'seller': sellerprofile } # provide your context here, if applicable
                    tmessage = render_to_string('sellerTransactionNotification.html', tcontext)

                    temail = EmailMultiAlternatives(tsubject, tmessage, 'trans@yedi.com.ng', [sellerprofile['email']], bcc=['hello.yedi@gmail.com'])
                    temail.attach_alternative(tmessage, 'text/html') # crucial part
                    temail.send()
                    print('Seller Emailed: '+ sellerprofile['email'])

                    if hasattr(sellerprofile, 'phonenumber'):
                        xx = urllib.request.urlopen("http://smsclone.com/index.php?option=com_spc&comm=spc_api&username="+textusr+"&password="+textpwd+"&sender="+tsender+"&recipient="+sellerprofile['phonenumber']+"&message="+transSMSmsg+"").read()
                        print (xx)
                        print('Seller SMSed: '+ sellerprofile['phonenumber'])

                # Update the document content
                # This can be done as you would any other dictionary
                neworder['sentemail'] = 'yes'
                neworder['senttext'] = 'yes'

                # You must save the document in order to update it on the database
                neworder.save()

                print(change)
    print('**************************************************************')




"""
class MyCronJob(CronJobBase):
    RUN_EVERY_MINS = 1 # every 2 hours

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'my_app.email_cron'    # a unique code

    def do(self):
        client = Cloudant('yadee', 'Tyeema01', account='yadee')
        client.connect()
        my_database = client['yadee_test']

        # Iterate over a "continuous" _db_updates feed with additional options
        changes = my_database.changes(feed='longpoll',  heartbeat=100, descending=False)
        for change in changes:
            if (change['id'].startswith('oc_0000uyu')): # | change['id'].startswith('op_')):
                send_mail(
                'Cron Email',
                'Here is the cron message. O.O',
                'hello@yedi.com.ng',
                ['hello.yedi@gmail.com'],
                fail_silently=False,
                )
                print(change)
"""