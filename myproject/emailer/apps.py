from django.apps import AppConfig


class EmailerConfig(AppConfig):
    name = 'emailer'
    verbose_name = 'A Much Better Name'

