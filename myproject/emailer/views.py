from django.shortcuts import render
from django.http import HttpResponse
from cloudant.client import Cloudant
import urllib.request
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from dateutil.parser import parse
from .models import ProductImage
from datetime import date
from django.views.decorators.csrf import csrf_exempt
# try:
#     from myproject import settings
# except Exception:
#     from myproject import settings
from django.conf import settings
from django.views.generic.list import ListView
from django.views import generic
from yadeebackend.models import Image, UserProfile, YediShop, User
from yadeebackend.emails import send_signup_email, send_welcomeback_email
from yadeebackend.pushnotifications import send_push_message
from yadeebackend.mailchimp_email import moveusers as mv, send_workflow_email


def singlepush(request):
    #ExponentPushToken[4guMhZFLtQO7T4WJFOCLY_]

    # send_push_message('ExponentPushToken[P030sQFRViO2ChHqmLyL5y]', "Tip: One product upload should be for one item only so customers don't get confused. Please re upload :)")

    return HttpResponse('warned seller')

def sendpushmessage(request):
    #ExponentPushToken[4guMhZFLtQO7T4WJFOCLY_]

    # for seller in UserProfile.objects.all():
    #     if 262 < seller.id < 316:
    #         try:
    #             send_push_message(seller.pushtoken, 'Tip: Having few products uploaded reduces your ranking for The Atrium Sale selection (unless you sell one unique product). Theres still time to upload more.')
    #             print('sent to:' + seller.user.username)
    #         except Exception as e:
    #             print('error: '+seller.user.username + ' ' + str(seller.id))
    return HttpResponse('warned all for few products')




def welcomeoldusers(request):

    import requests

    p = UserProfile.objects.get(id = 22)
    u = User.objects.get(id = 34)
    send_welcomeback_email(u, p)
    return HttpResponse('Done!')


def moveusers(request):

    #mv()
    send_workflow_email('commissions@yedi.com.ng', 'Teema Galaudu')

    #send_push_message('ExponentPushToken[VZfxh2J5S7Md6Q_omEnXOW]', 'Beautiful Ankara collections from Cheenapy and Sunny Fabric Stores. Open Yedi to check them out!')

    #SEND WELCOME OLD USERS EMAIL
    # f= open("welcomeemaillogs.txt","w+")
    # for profile in UserProfile.objects.all():
    #     if profile.id < 182:
    #         send_welcomeback_email(profile.user, profile)
    #         f.write('WELCOME EMAIL SENT TO: ' + profile.user.email + ' - ' + profile.user.username + '\r\n')
    #         print('WELCOME EMAIL SENT TO: ' + profile.user.email + ' - ' + profile.user.username )
    # f.close()

    #SEND FAILED SIGN UP EMAILS
    # p = UserProfile.objects.get(id = 215)
    # u = User.objects.get(id = 227)
    # send_signup_email(u, p)

    #SEND TEST SMS
    # textusr = 'tymah'
    # textpwd = 'tyeema'
    # sender = 'YediOrder'
    # tsender = 'YediTrans'
    # transSMSmsg= 'You+have+a+new+transaction+in+Yedi.+Goto+"Account+>+My+Transactions"+in+Yedi+to+change+transaction+status'
    # orderSMSmsg= "Thank+you+for+Ordering+from+Yedi.+You+can+track+progress+in+'My+Orders'."
    # xx = requests.get("https://smsclone.com/api/sms/dnd-fallback?username="+textusr+"&password="+textpwd+"&sender=YediApp&recipient=08033638686&message="+orderSMSmsg)
    #xx = requests.get('https://smsclone.com/api/sms/dnd-fallback?username=tymah&password=tyeema&sender=YediApp&recipient=09068822180&message=yeah')
    #print(xx)



    # data = requests.get('http://migration.yedi.com.ng/api/yedishoppers/')
    # shoppers = data.json()
    # for shopper in shoppers:
    #     if UserProfile.objects.filter(user__username = shopper['username']):
    #         usr = UserProfile.objects.filter(user__username = shopper['username'])[0]
    #         usr.phone = shopper['phonenumber']
    #         print('number changed: ' + str(usr.phone))
    #         usr.save()
    #     else:
    #         user= User(username=shopper['username'],
    #
    #                       email=shopper['email'],
    #             )
    #         user.set_password('temppass974694')
    #         user.is_staff = False
    #         user.save()
    #         profile = UserProfile(user=user, isSeller=True, phone=shopper['phonenumber'])
    #         profile.save()
    #
    #         print(user.username)
    return HttpResponse('Done!')

def tinifyimages(request):
    counter = 0
    import tinify
    tinify.key = "vDuqq-xghd2jx2Vn58cqV61LLxRK8Om3"
    for prodimg in Image.objects.all():
        # if hasattr(prodimg, "tinified"):
        #     if prodimg.tinified == True:
        counter = counter + 1
        fullurl = (settings.BASE_DIR+ prodimg.image.url)
        source = tinify.from_file(fullurl)
        resized = source.resize(
                        method="scale",
                        width=150,
                    )
        resized.to_file(fullurl)
        prodimg.save()
        print(prodimg.image.url +": "+str(counter))
        # return HttpResponse("at the tinified index after: " + str(counter)+ " Image(s)")
    return HttpResponse("at the tinified index after: " + str(counter) + " Image(s)")


def appleverify(request):
    import json
    from django.http import JsonResponse

    x= ' { "applinks": { "apps": [], "details": [{ "appID": "ng.com.app.yedi", "paths": ["/product/*", "/seller/*", "/open/*"]}]}} '
    y= '''
    {
      "applinks": {
        "apps": [],
        "details": [{
          "appID": "ng.com.app.yedi",
          "paths": ["/product/*", "/seller/*", "/open/*"]
        }]
      }
    }
    '''
    return JsonResponse(json.loads(x))

def androidverify(request):
    x= '''
    <application>

      <activity android:name=”MainActivity”>
        <intent-filter android:autoVerify="true">
          <action android:name="android.intent.action.VIEW" />
          <category android:name="android.intent.category.DEFAULT" />
          <category android:name="android.intent.category.BROWSABLE" />
          <data android:scheme="http" android:host="cdn2.yedi.com.ng" />
          <data android:scheme="https" />
        </intent-filter>
      </activity>
      <activity android:name=”SecondActivity”>
        <intent-filter>
          <action android:name="android.intent.action.VIEW" />
          <category android:name="android.intent.category.DEFAULT" />
          <category android:name="android.intent.category.BROWSABLE" />
          <data android:scheme="https" android:host="cdn2.yedi.com.ng" />
        </intent-filter>
      </activity>

    </application>
    '''
    return HttpResponse(x)

'''
cloudantDB = 'yadee_prod'
cloudantUsersDB = '_users'

def index(request):
    #print('_______________________________________________________________')
    #print('DB: ' + cloudantDB)
    #print('**************************************************************')
    #print('cron job running: Checking for new orders')
    client = Cloudant('yadee', 'Tyeema01', account='yadee')
    client.connect()
    my_database = client[cloudantDB]
    users_database = client['_users']
    textusr = 'tymah'
    textpwd = 'tyeema'
    sender = 'YediOrder'
    tsender = 'YediTrans'
    transSMSmsg= 'You+have+a+new+transaction+in+Yedi.+Goto+"Account+>+My+Transactions"+in+Yedi+to+change+transaction+status'
    orderSMSmsg= "Thank+you+for+Ordering+from+Yedi.+You+can+track+progress+in+'My+Orders'."

    # Iterate over a "continuous" _db_updates feed with additional options
    changes = my_database.changes(feed='longpoll',  heartbeat=100, descending=False)
    for change in changes:
        if change is not None:
            if (change['id'].startswith('oc_') | change['id'].startswith('op')):
                if(change['changes'][0]['rev'].startswith('1-')):# and not hasattr(change['changes'][0], 'deleted') ):# or not hasattr(change['changes'][0],'senttext')):
                    neworder = my_database[change['id']]
                    date = parse(neworder['orderdate'])
                    print( neworder['_id'])
                    print('NEW ORDER PLACED BY: '+ neworder['user']['fullname'])
                    #for product in neworder['products']:
                    #    product['dataimage'] = "data:image/jpeg;base64,"+product['image']
                        #email & text to buyer

                    subject = 'Your Yedi Order'
                    context = { 'neworder': neworder, 'id' : neworder['_id'], 'date': date } # provide your context here, if applicable
                    message = render_to_string('ps_invoice.html', context)

                    email = EmailMultiAlternatives(subject, message, 'orders@yedi.com.ng', [neworder['user']['email']], bcc=['hello.yedi@gmail.com'])
                    email.attach_alternative(message, 'text/html') # crucial part
                    email.send()
                    print('Shopper Emailed: '+ neworder['user']['email'])

                    if hasattr(neworder['user'], 'phonenumber'):
                        xx = urllib.request.urlopen("http://smsclone.com/index.php?option=com_spc&comm=spc_api&username="+textusr+"&password="+textpwd+"&sender="+sender+"&recipient="+neworder['user']['phonenumber']+"&message="+orderSMSmsg+"").read()
                        print ('Buyer SMSed ('+ neworder['user']['phonenumber']  +') with cost: '+str(xx))

                    #email and text to sellers
                    for product in neworder['products']:
                        #product['image'] = 'data:image/jpeg;base64,'+ product['image']

                        userprefix = 'org.couchdb.user:'+ product['vendor']
                        sellerprofile = users_database[userprefix]

                        tsubject = 'Your Yedi Transaction'
                        tcontext = { 'orderid': neworder['_id'], 'product': product, 'id' : product['_id'], 'seller': sellerprofile, 'imageurl': product['image']['url'] } # provide your context here, if applicable
                        tmessage = render_to_string('sellerTransactionNotification.html', tcontext)

                        temail = EmailMultiAlternatives(tsubject, tmessage, 'trans@yedi.com.ng', [sellerprofile['email']], bcc=['hello.yedi@gmail.com'])
                        temail.attach_alternative(tmessage, 'text/html') # crucial part
                        temail.send()
                        print('Seller Emailed: '+ sellerprofile['email'])
                        if hasattr(sellerprofile, 'phonenumber'):
                            xx = urllib.request.urlopen("http://smsclone.com/index.php?option=com_spc&comm=spc_api&username="+textusr+"&password="+textpwd+"&sender="+tsender+"&recipient="+sellerprofile['phonenumber']+"&message="+transSMSmsg+"").read()
                            print (xx)
                            print('Seller SMSed: '+ sellerprofile['phonenumber'])
                            neworder['senttext'] = 'yes'

                    # Update the document content
                    # This can be done as you would any other dictionary
                    neworder['sentemail'] = 'yes'


                    # You must save the document in order to update it on the database
                    neworder.save()

                    print('**************************************************************')
    #print('End of New order Checks')

    return HttpResponse("Hello, world. You're at the emailer index.")


def pwdreset(request):
    #print('**************************************************************')
    #print(' cron job running: Pwd reset Requests')
    client = Cloudant('yadee', 'Tyeema01', account='yadee')
    client.connect()
    my_database = client[cloudantDB]
    textusr = 'tymah'
    textpwd = 'tyeema'
    sender = 'YediPass'
    resetSMSmsg= "Your+Yedi+account+Password+Reset+Code+is:"
    pwdresetfile = []

    # Iterate over a "continuous" _db_updates feed with additional options
    changes = my_database.changes(feed='longpoll',  heartbeat=100, descending=False)
    for change in changes:
        if (change is not None and change['id'].startswith('pwdreset_')):
            if(change['changes'][0]['rev'].startswith('1-') ):# or not hasattr(change['changes'][0],'senttext')):
                pwdresetfile = my_database[change['id']]

                subject = 'Password Reset Request'
                context = { 'resetcode' : pwdresetfile['code']} # provide your context here, if applicable
                message = render_to_string('pwsreset.html', context)

                email = EmailMultiAlternatives(subject, message, 'Yedi Password Reset<donotreply@yedi.com.ng>', [pwdresetfile['email']], bcc=['hello.yedi@gmail.com'])
                email.attach_alternative(message, 'text/html') # crucial part
                email.send()
                print('Password Reset Request For: '+ pwdresetfile['email'])

                pwdresetfile['sentemail'] = 'yes'

                # You must save the document in order to update it on the database
                pwdresetfile.save()
                print('**************************************************************')
                #xx = urllib.request.urlopen("http://smsclone.com/index.php?option=com_spc&comm=spc_api&username="+textusr+"&password="+textpwd+"&sender="+sender+"&recipient="+neworder['user']['phonenumber']+"&message="+resetSMSmsg+"").read()
                #print ('Buyer SMSed with cost: '+str(xx))
    #print('End of Pwd request checks')
    #print('**************************************************************')
    return HttpResponse("Hello, world. You're at the PWD RST index.pwd changed for:"+str(pwdresetfile))


def newsignup(request):
    #print('**************************************************************')
    #print(' cron job running: Looking for new signups')
    client = Cloudant('yadee', 'Tyeema01', account='yadee')
    client.connect()
    my_database = client[cloudantDB]
    users_database = client[cloudantUsersDB]

    useremailfile = []

    # Iterate over a "continuous" _db_updates feed with additional options
    changes = my_database.changes(feed='longpoll',  heartbeat=100, descending=False)
    for change in changes:
        if (change is not None and change['id'].startswith('useremail_') ):
            if(change['changes'][0]['rev'].startswith('1-') ):# or not hasattr(change['changes'][0],'senttext')):
                useremailfile = my_database[change['id']]
                users_database.get('')
                userprefix = 'org.couchdb.user:'+ useremailfile['username']
                userprofile = users_database[userprefix]

                subject = 'Welcome to Yedi'
                context = { 'user': userprofile} # provide your context here, if applicable
                message = render_to_string('welcometoyedi.html', context)

                email = EmailMultiAlternatives(subject, message, 'Welcome to Yedi<donotreply@yedi.com.ng>', [useremailfile['email']], bcc=['hello.yedi@gmail.com'])
                email.attach_alternative(message, 'text/html') # crucial part
                email.send()
                useremailfile['sentemail'] = 'yes'
                print('New User Signed Up: '+ useremailfile['email'])

                useremailfile['sentemail'] = 'yes'

                # You must save the document in order to update it on the database
                useremailfile.save()
                print('**************************************************************')
                #xx = urllib.request.urlopen("http://smsclone.com/index.php?option=com_spc&comm=spc_api&username="+textusr+"&password="+textpwd+"&sender="+sender+"&recipient="+neworder['user']['phonenumber']+"&message="+resetSMSmsg+"").read()
                #print ('Buyer SMSed with cost: '+str(xx))
    #print('Done looking for new users')
    #print('**************************************************************')
    #print('_______________________________________________________________')

    return HttpResponse("Hello, world. You're at the New User index")


@csrf_exempt
def post_uploadimg(request):
    if request.method == 'GET':
        return HttpResponse("Uploader")
    elif request.method == 'POST':
        ProductImage(uploaddate = date.today(), image = request.FILES['file'] ).save()
        storedimg = settings.STATIC_URL+ "media/Images/ProductImages/"+(request.FILES['file']._name).replace('(','').replace(')','').replace('&','').replace(' ','_')
        return HttpResponse({storedimg})


@csrf_exempt
def getallproducts(request):
    if request.method == 'GET':

        return HttpResponse("catsss")
    elif request.method == 'POST':
        ProductImage(uploaddate = date.today(), image = request.FILES['file'] ).save()
        storedimg = settings.STATIC_URL+ "media/Images/ProductImages/"+(request.FILES['file']._name).replace('(','').replace(')','').replace('&','').replace(' ','_')
        return HttpResponse({storedimg})

@csrf_exempt
def post_deleteimg(request):
    if request.method == 'GET':
        return HttpResponse("Deleter")
    elif request.method == 'POST':
        x = ProductImage.objects.filter(image__url=request.FILES['file'])
        x.delete()
        return HttpResponse('deleted')
    return HttpResponse('deleted')

# from myproject.yadeebackend.models import Image
def tinifyimages(request):
    counter = 0
    import tinify
    tinify.key = "vDuqq-xghd2jx2Vn58cqV61LLxRK8Om3"
    for prodimg in ProductImage.objects.all():
    # for prodimg in Image.objects.all():
        # if hasattr(prodimg, "tinified"):
        #     if prodimg.tinified == True:
        counter = counter + 1
        fullurl = (settings.BASE_DIR+ prodimg.image.url)
        source = tinify.from_file(fullurl)
        resized = source.resize(
                        method="scale",
                        width=150,
                    )
        resized.to_file(fullurl)
        prodimg.tinified = True
        prodimg.save()
        print(prodimg.image.url +": "+str(counter))
    return HttpResponse("at the tinified index")



def transformtoSaleor(request):


    data = {}
    client = Cloudant('yadee', 'Tyeema01', account='yadee')
    client.connect()
    my_database = client[cloudantDB]

    for document in my_database:
        docid = document['_id']
        if docid.startswith('prod_'):
            currprod = my_database[docid]
            m = currprod.values()
            if 'wooed' not in currprod.keys():
                data['name'] = currprod['productname']
                data['regular_price'] = currprod['saleprice']
                attributes = []


def transformtoWoo(request):
    counter = 0
    from woocommerce import API

    wcapi = API(
        url="http://shop.yedi.com.ng",
        consumer_key="ck_e49d0de958bba5e2ee7f2dd942e296f753971f84",
        consumer_secret="cs_ebaf524843217e286d3a3401fbb580951f953ff0",
        wp_api=True,
        version="wc/v1"
    )

    data = {}
    client = Cloudant('yadee', 'Tyeema01', account='yadee')
    client.connect()
    my_database = client[cloudantDB]

    for document in my_database:
        docid = document['_id']
        if docid.startswith('prod_'):
            currprod = my_database[docid]
            m = currprod.values()
            if 'wooed' not in currprod.keys():
                data['name'] = currprod['productname']
                data['regular_price'] = currprod['saleprice']
                attributes = []

                if 'description' in currprod.keys():
                    data['description'] = currprod['description']
                if 'desc' in currprod.keys():

                    data['desc'] = currprod['desc']
                data['categories'] = currprod['categories']

                if 'dateadded' in currprod.keys():
                    data['date_created'] = currprod['dateadded']

                if 'colors' in currprod.keys():
                    #data['colorsarr'] = currprod['colors']
                    attributes.append({})
                    #attributes[len(attributes)-1]['options'] = ast.literal_eval(currprod['colors'])
                    dd = currprod['colors']
                    attributes[len(attributes)-1]['options'] = currprod['colors'].split(',')
                    attributes[len(attributes)-1]['visible'] = True
                    attributes[len(attributes)-1]['position'] = 0
                    attributes[len(attributes)-1]['name'] = 'Color'
                    data['attributes'] = attributes

                if 'sizes' in currprod.keys():
                    #data['colorsarr'] = currprod['colors']
                    attributes.append({})
                    attributes[len(attributes)-1]['options'] = currprod['sizes'].split(',')
                    #attributes[len(attributes)-1]['options'] = ast.literal_eval(currprod['sizes'])
                    attributes[len(attributes)-1]['visible'] = True
                    attributes[len(attributes)-1]['position'] = 0
                    attributes[len(attributes)-1]['name'] = 'Sizes'
                    data['attributes'] = attributes

                data['featured'] = currprod['featured']
                data['sku'] = currprod['_id']
                images = []
                for currimage in currprod['images']:
                    images.append({})
                    images[counter]['src'] = currimage['url']
                    images[counter]['position'] = counter
                    counter = counter + 1
                counter = 0
                data['images'] = images

                #print(currprod)
                print(wcapi.post("products", data).json())
                currprod['wooed'] = True
                currprod.save()

    return HttpResponse("at the wooing index")


class AllProductsView(generic.ListView):
    context_object_name = 'products'
    template_name = 'emailer/suitesAcc.html'
    def get_queryset(self):
        """Return the last five published questions."""
        return ProductImage.objects.order_by('number')



def moveusers(request):

    import urllib.request
    with urllib.request.urlopen('http://python.org/') as response:
        html = response.read()
        print(html)
        return HttpResponse("All users Moved")

    # req = urllib.request.Request('https://mutemailer.yedi.com.ng/api/users/')
    # with urllib.request.urlopen(req) as response:
    #     the_page = response.read()
    #     print(the_page)
    #     return HttpResponse("All users Moved")


def rfc(request):
    #print('**************************************************************')
    #print(' cron job running: Looking for new signups')
    client = Cloudant('yadee', 'Tyeema01', account='yadee')
    client.connect()
    my_database = client[cloudantDB]
    orderid = 'oc_00k173ff'
    orderfile = my_database[orderid]

    date = parse(orderfile['orderdate'])
    subject = orderid+' Status: Ready for Collection'
    context = { 'order': orderfile, 'id': orderfile['_id'], 'date':date} # provide your context here, if applicable
    message = render_to_string('readyforcollection.html', context)

    email = EmailMultiAlternatives(subject, message, 'Your Yedi Order<orders@yedi.com.ng>', [orderfile['user']['email']], bcc=['hello.yedi@gmail.com'])
    email.attach_alternative(message, 'text/html') # crucial part
    email.send()
    orderfile['readyforcollectionemail'] = 'yes'
    print('Ready for Collection Email Sent: '+ orderfile['user']['email'])

    # if hasattr(orderfile['user'], 'phonenumber'):
    #     xx = urllib.request.urlopen("http://smsclone.com/index.php?option=com_spc&comm=spc_api&username="+textusr+"&password="+textpwd+"&sender="+sender+"&recipient="+neworder['user']['phonenumber']+"&message="+orderSMSmsg+"").read()
    #     print ('Buyer SMSed ('+ orderfile['user']['phonenumber']  +') with cost: '+str(xx))


    # You must save the document in order to update it on the database
    #orderfile.save()
    print('**************************************************************')
    return HttpResponse("Hello, world. You're at the rfc index")
'''