from django.db import models


class ProductImage(models.Model):
    uploaddate = models.DateField(blank=True)
    image = models.ImageField(upload_to='Images/ProductImages')
    tinified = models.BooleanField(default=False)

    def __str__(self):
        return self.image.url

