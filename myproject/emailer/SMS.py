import requests
import rollbar



def send_neworder_sms(number, orderid):
    try:
        orderSMSmsg = "OrderID:+"+orderid+"+Thank+you+for+Ordering+from+Yedi.+Your+items+will+be+with+you+soon.+" \
                        "+You+can+monitor+progress+in+'My+Orders'."
        xx = requests.get('https://smsclone.com/api/sms/dnd-fallback?username=tymah&password=tyeema&sender=Yedi+App&recipient='+number+'&message='+ orderSMSmsg)
        return xx
    except Exception as e:
        rollbar.report_exc_info(extra_data={'error': e, 'message': 'couldnt send SMS to: ' +str(number)})


def send_transaction_sms(number, product, quantity):
    try:
        orderSMSmsg = "You+have+a+new+transaction+on+Yedi+App+for+"+product+"+("+str(quantity)+" pieces).+Please " \
                        "notify+your+buyer+of+its+availability+under+'My+Transactions'+in 'My+Account'" \
                         "+or+email+us+at+hello+at+yedi.com.ng"
        xx = requests.get('https://smsclone.com/api/sms/dnd-fallback?username=tymah&password=tyeema&sender=Yedi+App&recipient='+number+'&message='+ orderSMSmsg)
        return xx
    except Exception as e:
        rollbar.report_exc_info(extra_data={'error': e, 'message': 'couldnt send SMS to: ' +str(number)})


def send_resetcodepack_sms(code, number):
    try:
        resetSMSmsg = "Use+code+"+code+"+to+reset+your+password+on+Yedi+App"
        xx = requests.get('https://smsclone.com/api/sms/dnd-fallback?username=tymah&password=tyeema&sender=Yedi+App&recipient='+number+'&message='+ resetSMSmsg)
        return xx
    except Exception as e:
        rollbar.report_exc_info(extra_data={'error': e, 'message': 'couldnt send SMS to: ' + str(number)})


def send_custom_sms(message, number):
    try:
        message= message.replace(' ', '+')
        xx = requests.get('https://smsclone.com/api/sms/dnd-fallback?username=tymah&password=tyeema&sender=Yedi+App&recipient='+number+'&message='+ message)
        return xx
    except Exception as e:
        rollbar.report_exc_info(extra_data={'error': e, 'message': 'couldnt send SMS to: ' + str(number)})

