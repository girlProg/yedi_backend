from django.conf.urls import url
#
# try:
#     from myproject import settings
# except Exception:
#     from myproject import settings
from django.conf import settings
from django.conf.urls.static import static


from . import views

urlpatterns = [

    url(r'^tinify', views.tinifyimages, name='tinifier'),
    url(r'^moveusers', views.moveusers, name='moveusers'),
    url(r'^sendpushmessage', views.sendpushmessage, name='sendpushmessage'),
    url(r'^singlepush', views.singlepush, name='singlepush'),
    url(r'^apple-app-site-association', views.appleverify, name='appleverify'),
    url(r'^assetlinks.json', views.androidverify, name='androidverify'),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)



    '''
    url(r'^$', views.index, name='index'),
    url(r'^pwdrst$', views.pwdreset, name='pwdrst'),
    url(r'^newsignup', views.newsignup, name='newsignup'),
    url(r'^uploader', views.post_uploadimg, name='uploader'),
    url(r'^getallprods', views.getallproducts, name='allprods'),
    url(r'^deleter', views.post_deleteimg, name='deleter'),

    url(r'^rfc', views.rfc, name='readyforcollection'),
    url(r'^moveusers', views.moveusers, name='moveusers'),
    url(r'^transformtowoo', views.transformtoWoo, name='transformtowoo'),
    '''