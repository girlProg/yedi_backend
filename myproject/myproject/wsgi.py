import os
import sys


# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myproject.myproject.settings")


# os.environ['DJANGO_SETTINGS_MODULE'] = 'myproject.myproject.settings'
os.environ['DJANGO_SETTINGS_MODULE'] = 'myproject.settings'


from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
